#!/usr/bin/python
import os.path
import time
import shutil

FILE = '/opt/sdlpop/QUICKSAVE.SAV'

fileno = 400

while True:
    if os.path.exists(FILE):
        new_filename = '%04d.SAV' % fileno
        fileno += 1
        shutil.move(FILE, new_filename)
        print("New file is %s." % new_filename)
    time.sleep(1)
