#!/usr/bin/python

import sys


def ConvertOne(x):
    if x == '.':
        return 'Nothing'
    if x == 'S':
        return 'Shift'
    return x


print(', '.join(['Move::%s' % ConvertOne(x)
                 for x in sys.stdin.read().split()]))
