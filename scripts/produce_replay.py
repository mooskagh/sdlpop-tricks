#!/usr/bin/python

import sdlpop
import sys

x = sdlpop.Replay()
x.Load('/home/crem/dev/libsdlpop/scripts/empty.p1r')
x.savegame.Load(sys.argv[1])
x.savegame.curr_tick = 1
with open(sys.argv[2]) as f:
    keypresses = f.read().strip()
    print(keypresses)
    x.keypresses.Update('. ' + keypresses + ' 100')
x.saved_random_seed = x.savegame.random_seed
x.Save('/opt/sdlpop/replays/generated.p1r')
