#!/bin/bash

SRCDIR=../SDLPoP/src/
DSTDIR=../src/sdlpop.orig/

for file in $SRCDIR/*.c $SRCDIR/*.h; do
  name=$(basename $file)
  unifdef -f cleanup-macros.txt $file > $DSTDIR$name
  clang-format -i $DSTDIR$name
done
