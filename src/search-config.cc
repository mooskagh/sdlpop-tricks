#include "search-config.h"

#include <iostream>

const std::string kRootDir = "/home/crem/dev/libsdlpop/";

namespace {

std::pair<int, int> YMaskToCoords(uint8_t y_bitmask) {
  int y_from = -100;
  int y_to = 300;
  switch (y_bitmask) {
    case 1:
      y_to = 65;
      break;
    case 2:
      y_from = 66;
      y_to = 128;
      break;
    case 3:
      y_to = 128;
      break;
    case 4:
      y_from = 129;
      break;
    case 6:
      y_from = 66;
      break;
    case 0x3f:
      break;
    default:
      std::cerr << "Unexpected Y bitmask: " << int(y_bitmask) << "\n";
      exit(1);
  }
  return {y_from, y_to};
}

}  // namespace

SearchConfig::Rule& SearchConfig::AddRuleFollowing(uint8_t room, Direction dir,
                                                   DoorOpen door_open,
                                                   int8_t x1, int8_t x2,
                                                   uint8_t y_bitmask) {
  auto [y_from, y_to] = YMaskToCoords(y_bitmask);
  const int following = rules.size();
  return rules.emplace_back()
      .WithRoom(room)
      .WithDirection(dir)
      .WithXRange(x1, x2)
      .WithYRange(y_from, y_to)
      .WithFollowing(following);
}

SearchConfig::Rule& SearchConfig::AddRule(uint8_t room, Direction dir,
                                          DoorOpen door_open, int8_t x1,
                                          int8_t x2, uint8_t y_bitmask) {
  auto [y_from, y_to] = YMaskToCoords(y_bitmask);
  return rules.emplace_back()
      .WithRoom(room)
      .WithDirection(dir)
      .WithDoorOpen(door_open)
      .WithXRange(x1, x2)
      .WithYRange(y_from, y_to);
}

SearchConfig::Rule& SearchConfig::AddRule() { return rules.emplace_back(); }

SearchConfig::Rule& SearchConfig::AddRule2(uint8_t room, Direction dir,
                                           DoorOpen door_open, int8_t x1,
                                           int8_t x2, int y_from, int y_to) {
  return rules.emplace_back()
      .WithRoom(room)
      .WithDirection(dir)
      .WithDoorOpen(door_open)
      .WithXRange(x1, x2)
      .WithYRange(y_from, y_to);
}

SearchConfig::Rule& SearchConfig::AddRuleWithGuard(uint8_t room, Direction dir,
                                                   DoorOpen door_open) {
  return rules.emplace_back()
      .WithRoom(room)
      .WithDirection(dir)
      .WithDoorOpen(door_open)
      .WithGuard();
}

void SearchConfig::AddLossRule(uint8_t room, uint8_t y_bitmask, int from_x,
                               int to_x) {
  auto [y_from, y_to] = YMaskToCoords(y_bitmask);
  loss_rules.push_back({room, from_x, to_x, y_from, y_to});
}

void SearchConfig::AddLossRule(uint8_t room, int from_x, int to_x, int y_from,
                               int y_to) {
  loss_rules.push_back({room, from_x, to_x, y_from, y_to});
}

SearchConfig::Rule& SearchConfig::Rule::WithRoom(uint8_t x) {
  room = x;
  return *this;
}
SearchConfig::Rule& SearchConfig::Rule::WithDirection(Direction x) {
  direction = x;
  return *this;
}
SearchConfig::Rule& SearchConfig::Rule::WithDoorOpen(DoorOpen x) {
  door_open = x;
  return *this;
}
SearchConfig::Rule& SearchConfig::Rule::WithXRange(int8_t from, int8_t to) {
  x_from = from;
  x_to = to;
  return *this;
}
SearchConfig::Rule& SearchConfig::Rule::WithYRange(int from, int to) {
  y_from = from;
  y_to = to;
  return *this;
}
SearchConfig::Rule& SearchConfig::Rule::WithTrob(uint16_t x) {
  trob = x;
  return *this;
}
SearchConfig::Rule& SearchConfig::Rule::WithGuard() {
  must_have_guard = true;
  return *this;
}
SearchConfig::Rule& SearchConfig::Rule::WithUseGuard() {
  must_have_guard = true;
  use_guard_coords = true;
  return *this;
}
SearchConfig::Rule& SearchConfig::Rule::WithFollowing(int x) {
  min_prev_position = x;
  return *this;
}
SearchConfig::Rule& SearchConfig::Rule::WithMinGuardHp(int x) {
  min_guard_hp = x;
  return *this;
}
SearchConfig::Rule& SearchConfig::Rule::WithMinKidHp(int x) {
  min_kid_hp = x;
  return *this;
}