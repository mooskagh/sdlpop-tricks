#include <absl/container/flat_hash_map.h>
#include <absl/container/flat_hash_set.h>

#include <string>
#include <vector>

#include "prince.h"
#include "scorer.h"
#include "search-config.h"

#define DEBUG_TAINT
struct FrameData {
  ScoreResult static_score;
  float dynamic_score;
  float score;
  int lowest_rank;
  std::string moves;
  std::string snapshot;
#ifdef DEBUG_TAINT
  bool tainted = false;
#endif

  const bool operator<(const FrameData& other) const {
    return score > other.score;
  }
  FrameData(ScoreResult static_score, float dynamic_score, float score,
            int lowest_rank, std::string snapshot, const std::string& mvs,
            char next_move)
      : static_score(static_score),
        dynamic_score(dynamic_score),
        score(score),
        lowest_rank(lowest_rank),
        moves(mvs),
        snapshot(std::move(snapshot)) {
    moves.push_back(next_move);
  }
};

struct IterationResult {
  bool win = false;
  float worst_score = 100000.0f;
  int hash_collisions = 0;
  int losses = 0;
  int candidates = 0;
  int largest_kid = 0;
  int global_largest_kid = 0;
  int novel_positions = 0;
};

class Search {
 public:
  Search(int argc, char* argv[], const SearchConfig& config);

  void Run(int limit);
  IterationResult DoOneFrame(int frame);

 private:
  void DumpState(const std::string& filename, int only_room = -1);

  Prince prince_;
  Scorer scorer_;
  std::string base_state_;
  absl::flat_hash_set<uint64_t> hashes_;
  absl::flat_hash_map<uint64_t, int> kid_counts_;
  absl::flat_hash_map<uint64_t, int> global_kid_counts_;
  std::vector<FrameData> next_frames_;
  std::vector<FrameData> cur_frames_;
  SearchConfig config_;
};
