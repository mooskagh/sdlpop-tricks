#pragma once

#include <string>
#include <utility>
#include <vector>

#include "search-config.h"

enum class Move {
  Nothing = 0,
  R = 0x01,
  L = 0x03,
  D = 0x04,
  RD = R | D,
  LD = L | D,
  U = 0x0c,
  RU = R | U,
  LU = L | U,
  Shift = 0x10,
  SR = Shift | R,
  SL = Shift | L,
  SD = Shift | D,
  SRD = Shift | RD,
  SLD = Shift | LD,
  SU = Shift | U,
  SRU = Shift | RU,
  SLU = Shift | LU,
  Restart = 0x20,
  SoundOff = 0x40,
};

std::string MovesToStr(const std::string& moves);

struct ScoreResult {
  float score = 0.0f;
  float position_score = 0.0f;
  int best_rule = 0;
};

class Scorer {
 public:
  Scorer(const SearchConfig& config) : config_(config) {}
  ScoreResult Score(const ScoreResult& prev_score) const;
  std::pair<float, int> PositionScore(const ScoreResult& prev_score) const;
  bool IsLoss() const;
  bool IsWin() const;
  void PositionDebugPrint() const;

 private:
  bool IsCustomLoss() const;

  const SearchConfig config_;
};
