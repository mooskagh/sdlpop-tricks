#pragma once

#include <fstream>
#include <optional>
#include <string>

class Writer {
 public:
  Writer(const std::string& filename) : file_(filename.c_str()) {}
  void Write(const std::string& str) {
    size_t sz = str.size();
    file_.write(reinterpret_cast<char*>(&sz), sizeof(sz));
    file_.write(str.data(), str.size());
  }

 private:
  std::ofstream file_;
};

class Reader {
 public:
  Reader(const std::string& filename) : file_(filename.c_str()) {}
  std::optional<std::string> Read() {
    size_t sz = 0;
    file_.read(reinterpret_cast<char*>(&sz), sizeof(sz));
    std::string result(sz, 'X');
    file_.read(&result[0], sz);
    if (file_) return result;
    return std::nullopt;
  }

 private:
  std::ifstream file_;
};
