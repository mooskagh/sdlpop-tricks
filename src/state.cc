#include "state.h"

#include <fstream>
#include <iostream>

#include "metrohash64.h"
#include "sdlpop/common.h"

namespace {

char quick_control[] = "........";

template <class T>
void AddItem(std::vector<State::Item>* dest, T& val, State::ItemType type) {
  dest->push_back({&val, sizeof(val), type});
}

std::vector<State::Item> GenerateItemsMap() {
  std::vector<State::Item> dest;
  AddItem(&dest, quick_control, State::ONLY_QUICKSAVE);
  AddItem(&dest, level, State::HASHABLE_MANUAL);
  AddItem(&dest, checkpoint, State::BASE_LAYER);
  AddItem(&dest, upside_down, State::PER_FRAME_STATE);
  AddItem(&dest, drawn_room, State::HASHABLE);
  AddItem(&dest, current_level, State::BASE_LAYER);
  AddItem(&dest, next_level, State::BASE_LAYER);
  AddItem(&dest, mobs_count, State::HASHABLE_MANUAL);
  AddItem(&dest, mobs, State::HASHABLE_MANUAL);
  AddItem(&dest, trobs_count, State::HASHABLE_MANUAL);
  AddItem(&dest, trobs, State::HASHABLE_MANUAL);
  AddItem(&dest, leveldoor_open, State::HASHABLE);
  AddItem(&dest, exit_room_timer, State::ONLY_STATE);
  AddItem(&dest, jumped_through_mirror, State::ONLY_STATE);
  // kid
  AddItem(&dest, Kid, State::HASHABLE);
  AddItem(&dest, hitp_curr, State::PER_FRAME_STATE);
  AddItem(&dest, hitp_max, State::PER_FRAME_STATE);
  AddItem(&dest, hitp_beg_lev, State::BASE_LAYER);
  AddItem(&dest, grab_timer, State::HASHABLE);
  AddItem(&dest, holding_sword, State::HASHABLE);
  AddItem(&dest, united_with_shadow, State::HASHABLE);
  AddItem(&dest, have_sword, State::HASHABLE);
  /*AddItem(&dest, ctrl1_forward, State::HASHABLE);
  AddItem(&dest, ctrl1_backward, State::HASHABLE);
  AddItem(&dest, ctrl1_up, State::HASHABLE);
  AddItem(&dest, ctrl1_down, State::HASHABLE);
  AddItem(&dest, ctrl1_shift2, State::HASHABLE);*/
  AddItem(&dest, kid_sword_strike, State::HASHABLE);
  AddItem(&dest, pickup_obj_type, State::HASHABLE);
  AddItem(&dest, offguard, State::HASHABLE);
  // guard
  AddItem(&dest, Guard, State::PER_FRAME_STATE);
  AddItem(&dest, Char, State::PER_FRAME_STATE);
  AddItem(&dest, Opp, State::PER_FRAME_STATE);
  AddItem(&dest, guardhp_curr, State::HASHABLE);
  AddItem(&dest, guardhp_max, State::PER_FRAME_STATE);
  AddItem(&dest, demo_index, State::BASE_LAYER);
  AddItem(&dest, demo_time, State::BASE_LAYER);
  AddItem(&dest, curr_guard_color, State::PER_FRAME_STATE);
  AddItem(&dest, guard_notice_timer, State::HASHABLE);
  AddItem(&dest, guard_skill, State::HASHABLE);
  AddItem(&dest, shadow_initialized, State::PER_FRAME_STATE);
  AddItem(&dest, guard_refrac, State::HASHABLE);
  AddItem(&dest, justblocked, State::HASHABLE);
  AddItem(&dest, droppedout, State::HASHABLE);
  // collision
  AddItem(&dest, curr_row_coll_room, State::PER_FRAME_STATE);
  AddItem(&dest, curr_row_coll_flags, State::PER_FRAME_STATE);
  AddItem(&dest, below_row_coll_room, State::PER_FRAME_STATE);
  AddItem(&dest, below_row_coll_flags, State::PER_FRAME_STATE);
  AddItem(&dest, above_row_coll_room, State::PER_FRAME_STATE);
  AddItem(&dest, above_row_coll_flags, State::PER_FRAME_STATE);
  AddItem(&dest, prev_collision_row, State::PER_FRAME_STATE);
  // flash
  AddItem(&dest, flash_color, State::PER_FRAME_STATE);
  AddItem(&dest, flash_time, State::PER_FRAME_STATE);
  // sounds
  AddItem(&dest, need_level1_music, State::PER_FRAME_STATE);
  AddItem(&dest, is_screaming, State::HASHABLE);
  AddItem(&dest, is_feather_fall, State::HASHABLE);
  AddItem(&dest, last_loose_sound, State::HASHABLE);
  // AddItem(&dest, next_sound, State::HASHABLE);
  // AddItem(&dest, current_sound, State::HASHABLE);
  // random
  AddItem(&dest, random_seed, State::PER_FRAME_STATE);
  // remaining time
  AddItem(&dest, rem_min, State::PER_FRAME_STATE);
  AddItem(&dest, rem_tick, State::PER_FRAME_STATE);
  // saved controls
  AddItem(&dest, control_x, State::PER_FRAME_STATE);
  AddItem(&dest, control_y, State::PER_FRAME_STATE);
  AddItem(&dest, control_shift, State::PER_FRAME_STATE);
  AddItem(&dest, control_forward, State::PER_FRAME_STATE);
  AddItem(&dest, control_backward, State::PER_FRAME_STATE);
  AddItem(&dest, control_up, State::PER_FRAME_STATE);
  AddItem(&dest, control_down, State::PER_FRAME_STATE);
  AddItem(&dest, control_shift2, State::PER_FRAME_STATE);
  AddItem(&dest, ctrl1_forward, State::PER_FRAME_STATE);
  AddItem(&dest, ctrl1_backward, State::PER_FRAME_STATE);
  AddItem(&dest, ctrl1_up, State::PER_FRAME_STATE);
  AddItem(&dest, ctrl1_down, State::PER_FRAME_STATE);
  AddItem(&dest, ctrl1_shift2, State::PER_FRAME_STATE);
  // replay recording state
  AddItem(&dest, curr_tick, State::PER_FRAME_STATE);
  return dest;
}

}  // namespace

State::State() : items_(GenerateItemsMap()) {}

void State::Quickload(const std::string& filename) {
  std::ifstream fi(filename.c_str());
  for (const auto& item : items_) {
    if (item.type != ONLY_STATE) {
      fi.read(reinterpret_cast<char*>(item.ptr), item.size);
    }
  }
  restore_room_after_quick_load();
  // update_screen();
}

void State::Quicksave(const std::string& filename) {
  std::ofstream fo(filename.c_str());
  for (const auto& item : items_) {
    if (item.type != ONLY_STATE) {
      fo.write(reinterpret_cast<char*>(item.ptr), item.size);
    }
  }
}

uint64_t State::KidHash() const {
  uint64_t hash;
  MetroHash64::Hash(reinterpret_cast<uint8_t*>(&Kid), sizeof(Kid),
                    reinterpret_cast<uint8_t*>(&hash), 1);
  return hash;
}

uint64_t State::ComputeHash() const {
  MetroHash64 hash;
  for (const auto& item : items_) {
    if (item.type == HASHABLE) {
      hash.Update(item.ptr, item.size);
    }
  }

  // manual hashes
  // hash.Update(level.fg);
  for (const uint8_t x : level.fg) {
    hash.Update(uint8_t(x & 0x1f));
  }

  hash.Update(level.guards_x);
  hash.Update(level.guards_dir);
  hash.Update(mobs, sizeof(mob_type) * mobs_count);
  if (Guard.alive) hash.Update(Guard);

  for (int i = 0; i < trobs_count; ++i) {
    const auto& trob = trobs[i];
    const auto idx = (trob.room - 1) * 30 + trob.tilepos;
    const auto type = level.fg[idx] & 0x1f;
    switch (type) {
      case tiles_0_empty:
      case tiles_1_floor:
      case tiles_3_pillar:
      case tiles_5_stuck:
      case tiles_6_closer:
      case tiles_7_doortop_with_floor:
      case tiles_8_bigpillar_bottom:
      case tiles_9_bigpillar_top:
      case tiles_10_potion:
      case tiles_12_doortop:
      case tiles_13_mirror:
      case tiles_14_debris:
      case tiles_15_opener:
      case tiles_17_level_door_right:
      case tiles_19_torch:
      case tiles_20_wall:
      case tiles_21_skeleton:
      case tiles_22_sword:
      case tiles_23_balcony_left:
      case tiles_24_balcony_right:
      case tiles_25_lattice_pillar:
      case tiles_26_lattice_down:
      case tiles_27_lattice_small:
      case tiles_28_lattice_left:
      case tiles_29_lattice_right:
      case tiles_30_torch_with_debris:
        break;
      case tiles_11_loose:
      case tiles_16_level_door_left:
      case tiles_18_chomper:
      case tiles_2_spike:
      case tiles_4_gate:
        hash.Update(trob);
        hash.Update(level.bg[idx]);
        hash.Update(level.fg[idx]);
        break;
      default:
        std::cerr << "Unknown trob type: " << int(type) << std::endl;
    }
  }

  uint64_t result;
  hash.Finalize(reinterpret_cast<uint8_t*>(&result));
  return result;
}

void State::LoadBase(const std::string& data) {
  const char* ptr = data.data();
  for (const auto& item : items_) {
    if (item.type == BASE_LAYER) {
      memcpy(item.ptr, ptr, item.size);
      ptr += item.size;
    }
  }
}

std::string State::SaveBase() const {
  std::string res;
  for (const auto& item : items_) {
    if (item.type == BASE_LAYER) {
      res.append(reinterpret_cast<const char*>(item.ptr), item.size);
    }
  }
  return res;
}

void State::LoadFrame(const std::string& data) {
  const char* ptr = data.data();
  for (const auto& item : items_) {
    if (item.type > BASE_LAYER) {
      memcpy(item.ptr, ptr, item.size);
      ptr += item.size;
    }
  }
  // restore_room_after_quick_load();
}

std::string State::SaveFrame() const {
  constexpr size_t kExpectedSize = 2691;
  std::string res;
  res.reserve(kExpectedSize);
  for (const auto& item : items_) {
    if (item.type > BASE_LAYER) {
      res.append(reinterpret_cast<const char*>(item.ptr), item.size);
    }
  }
  if (res.size() != kExpectedSize) {
    std::cerr << "Expected " << kExpectedSize << ", got " << res.size() << "\n";
  }
  return res;
}
