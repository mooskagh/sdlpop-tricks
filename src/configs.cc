#include "configs.h"

#include "search-config.h"

SearchConfig Level1Config() {
  using Direction = SearchConfig::Direction;
  using DoorOpen = SearchConfig::DoorOpen;
  SearchConfig config;

  config.run_name = "lvl1";
  config.AddRule(2, Direction::ToRight, DoorOpen::No);
  config.AddRule(3, Direction::ToRight, DoorOpen::No);
  config.AddRule(9, Direction::ToLeft, DoorOpen::No, 0, 9, 0x2);
  config.AddRule(9, Direction::ToCenter, DoorOpen::Yes, 0, 9, 0x2);

  config.search_width = 100000;
  config.novelty_boost = 0.5f;

  config.dup_score = -0.05f;
  config.global_dup_score = -0.001f;
  config.score_decay = 0.3f;

  return config;
}

SearchConfig Level2Config() {
  using Direction = SearchConfig::Direction;
  using DoorOpen = SearchConfig::DoorOpen;
  SearchConfig config;

  config.run_name = "lvl2";
  // config.run_name = "lvl2-nosword-4G";
  config.AddRule(4, Direction::ToLeft, DoorOpen::No);
  config.AddRule(6, Direction::ToLeft, DoorOpen::No, 1, 9, 0x2);
  config.AddRule(6, Direction::ToLeft, DoorOpen::No, 0, 9, 0x1);
  config.AddRule(1, Direction::ToLeft, DoorOpen::No, 0, 9, 0x1);
  config.AddRule(22, Direction::ToLeft, DoorOpen::No);
  config.AddRule(11, Direction::ToLeft, DoorOpen::No);
  config.AddRule(18, Direction::ToLeft, DoorOpen::No);
  config.AddRule(7, Direction::ToLeft, DoorOpen::No);
  config.AddRule(13, Direction::ToLeft, DoorOpen::No);
  config.AddRule(15, Direction::ToLeft, DoorOpen::No);
  config.AddRule(16, Direction::ToLeft, DoorOpen::No);
  config.AddRule(19, Direction::ToLeft, DoorOpen::No);
  config.AddRule(21, Direction::ToLeft, DoorOpen::No);
  config.AddRule(2, Direction::ToLeft, DoorOpen::No);
  config.AddRule(9, Direction::ToLeft, DoorOpen::No);
  config.AddRule(9, Direction::ToRight, DoorOpen::Yes);
  config.AddRule(23, Direction::ToCenter, DoorOpen::Yes);

  config.search_width = 333333;
  config.novelty_boost = 0.5f;

  config.dup_score = -0.05f;
  config.global_dup_score = -0.001f;
  config.score_decay = 0.7f;

  return config;
}

SearchConfig Level3Config() {
  using Direction = SearchConfig::Direction;
  using DoorOpen = SearchConfig::DoorOpen;
  SearchConfig config;

  config.run_name = "lvl3";
  config.AddRule(9, Direction::ToRight);
  config.AddRule(12, Direction::No, DoorOpen::No, 0, 3, 0x4);
  config.AddRule(12, Direction::ToRight, DoorOpen::No, 0, 9, 0x3);
  config.AddRule(11, Direction::ToRight, DoorOpen::No, 5, 9, 0x4);
  config.AddRule(11, Direction::ToRight, DoorOpen::No, 5, 9, 0x2);
  config.AddRule(11, Direction::ToRight, DoorOpen::No, 0, 9, 0x1);
  config.AddRule(14, Direction::ToRight, DoorOpen::No, 0, 9, 0x3f);
  config.AddRule(14, Direction::ToLeft, DoorOpen::No, 0, 9, 0x3f)
      .WithTrob(0x0209);
  config.AddRule(11, Direction::ToLeft, DoorOpen::No, 0, 9, 0x3f)
      .WithTrob(0x0209);
  config.AddRule(10, Direction::ToLeft, DoorOpen::No, 0, 9, 0x3f)
      .WithTrob(0x0209);
  config.AddRule(7, Direction::ToLeft, DoorOpen::No, 0, 9, 0x3)
      .WithTrob(0x0209);
  config.AddRule(2, Direction::ToLeft, DoorOpen::No, 0, 9, 0x3);
  config.AddRule(1, Direction::ToLeft, DoorOpen::No);
  config.AddRule(1, Direction::ToLeft, DoorOpen::No, 0, 4, 0x1);
  config.AddRule(5, Direction::ToLeft, DoorOpen::No, 0, 9, 0x1);
  config.AddRule(5, Direction::No, DoorOpen::No, 0, 4, 0x2);
  config.AddRule(5, Direction::ToLeft, DoorOpen::No, 0, 4, 0x4);
  config.AddRule(6, Direction::ToRight, DoorOpen::No, 0, 9, 0x1);
  config.AddRule(6, Direction::ToLeft, DoorOpen::Yes, 0, 9, 0x1);
  config.AddRule(5, Direction::No, DoorOpen::Yes, 0, 4, 0x4);
  config.AddRule(5, Direction::No, DoorOpen::Yes, 0, 4, 0x2);
  config.AddRule(5, Direction::ToRight, DoorOpen::Yes, 0, 9, 0x1);
  config.AddRule(1, Direction::ToRight, DoorOpen::Yes, 0, 8);
  config.AddRule(3, Direction::ToLeft, DoorOpen::Yes);
  config.AddRule(6, Direction::ToCenter, DoorOpen::Yes, 0, 9, 0x6);

  config.search_width = 333333;
  config.novelty_boost = 0.5f;

  // config.search_width = 33333;
  // config.novelty_boost = 100.0f;

  config.dup_score = -0.05f;
  config.global_dup_score = -0.001f;
  config.score_decay = 0.7f;

  return config;
}

SearchConfig Level4Config() {
  using Direction = SearchConfig::Direction;
  using DoorOpen = SearchConfig::DoorOpen;
  SearchConfig config;

  config.run_name = "lvl4";
  config.AddRule(1, Direction::ToRight);
  config.AddRule(2, Direction::ToRight);
  config.AddRule(5, Direction::ToRight);
  config.AddRule(6, Direction::ToRight);
  config.AddRule(24, Direction::ToRight, DoorOpen::No, 0, 9, 0x3);
  config.AddRule(7, Direction::ToRight, DoorOpen::No);
  config.AddRule(4, Direction::ToRight, DoorOpen::No);
  config.AddRule(11, Direction::ToRight, DoorOpen::No);
  config.AddRule(11, Direction::ToLeft, DoorOpen::No, 0, 9, 0x2);
  config.AddRule(4, Direction::ToLeft, DoorOpen::No, 5, 9, 0x2);
  config.AddRule(4, Direction::ToRight, DoorOpen::Yes, 4, 9, 0x2);
  config.AddRule(11, Direction::ToCenter, DoorOpen::Yes);
  config.AddRule(11, Direction::ToLeft, DoorOpen::Yes, 0, 9, 0x1);
  config.AddRule(4, Direction::ToLeft, DoorOpen::Yes, 0, 9, 0x1);
  config.AddRule(4, Direction::ToLeft, DoorOpen::Yes, 0, 3, 0x2);
  config.AddRule(7, Direction::ToLeft, DoorOpen::Yes);
  config.AddRule(24, Direction::ToLeft, DoorOpen::Yes, 2, 9);

  config.search_width = 1111111;
  config.novelty_boost = 0.5f;

  config.dup_score = -0.05f;
  config.global_dup_score = -0.003f;
  config.score_decay = 0.4f;

  return config;
}

SearchConfig Level5Config() {
  using Direction = SearchConfig::Direction;
  using DoorOpen = SearchConfig::DoorOpen;
  SearchConfig config;

  config.run_name = "lvl5";
  config.AddRule(7, Direction::ToRight);
  config.AddRule(9, Direction::ToLeft, DoorOpen::No).WithUseGuard();
  config.AddRuleWithGuard(7, Direction::ToLeft);
  config.AddRule(8, Direction::ToLeft, DoorOpen::No, 0, 5);
  config.AddRule(12, Direction::ToLeft);
  config.AddRule(2, Direction::ToLeft);
  config.AddRule(16, Direction::ToLeft);
  config.AddRule(15, Direction::ToLeft);
  config.AddRule(18, Direction::ToCenter);

  config.search_width = 444444;
  config.novelty_boost = 0.5f;

  config.dup_score = -0.02f;
  config.global_dup_score = -0.000001f;
  config.score_decay = 0.5f;

  return config;
}

SearchConfig Level6Config() {
  using Direction = SearchConfig::Direction;
  using DoorOpen = SearchConfig::DoorOpen;
  SearchConfig config;

  config.run_name = "lvl6";
  config.AddRule(24, Direction::ToLeft);
  config.AddRule(2, Direction::ToLeft);
  config.AddRule(15, Direction::ToLeft);
  config.AddRule(6, Direction::ToLeft);
  config.AddRule(18, Direction::ToLeft);
  config.AddRule(1, Direction::ToLeft);

  config.search_width = 666666;
  config.novelty_boost = 0.5f;

  config.dup_score = -0.02f;
  config.global_dup_score = -0.000001f;
  config.score_decay = 0.4f;

  return config;
}

/*AddRule(24, Direction::ToLeft);
AddRule(2, Direction::ToLeft);
AddRule(15, Direction::ToLeft);
AddRule(6, Direction::ToLeft);
AddRule(18, Direction::ToLeft);
AddRule(1, Direction::ToLeft); */

SearchConfig Level7FallConfig() {
  using Direction = SearchConfig::Direction;
  using DoorOpen = SearchConfig::DoorOpen;
  SearchConfig config;

  config.run_name = "lvl7-noseed";
  // config.AddLossRule(7, 0x2);
  config.AddRule(1, Direction::ToRight);
  config.AddRule(2, Direction::ToLeft);
  config.AddRule(1, Direction::No, DoorOpen::No, 0, 9, 0x6);
  config.AddRuleFollowing(3, Direction::ToLeft);
  config.AddRule(3, Direction::ToRight, DoorOpen::No, 0, 9, 0x3f)
      .WithTrob(0x031a);
  config.AddRule(3, Direction::ToLeft, DoorOpen::Yes);

  // config.search_width = 1111111;
  config.search_width = 15000;
  // config.search_width = 6;
  config.novelty_boost = 1.5f;

  config.dup_score = -0.05f;
  config.global_dup_score = -0.001f;
  config.score_decay = 0.2f;
  return config;
}

SearchConfig Level7Config() {
  using Direction = SearchConfig::Direction;
  using DoorOpen = SearchConfig::DoorOpen;
  SearchConfig config;

  int guard_health = 1;
  int kid_health = 1;

  config.run_name = "lvl7";
  // config.run_name = "lvl7-nosword";
  // config.AddLossRule(7, 0x2);
  config.AddRule(1, Direction::ToRight);
  config.AddRule(1, Direction::ToRight)
      .WithUseGuard()
      .WithMinGuardHp(guard_health)
      .WithMinKidHp(kid_health);
  config.AddRule(2, Direction::ToRight)
      .WithMinGuardHp(guard_health)
      .WithMinKidHp(kid_health);
  config.AddRule(2, Direction::ToRight)
      .WithUseGuard()
      .WithMinGuardHp(guard_health)
      .WithMinKidHp(kid_health);
  // config.AddRule(7, Direction::ToLeft);
  config.AddRule(7, Direction::ToCenter)
      .WithUseGuard()
      .WithMinGuardHp(guard_health)
      .WithMinKidHp(kid_health);
  config.AddRule(7, Direction::ToCenter, DoorOpen::Any, 0, 9, 0x2)
      .WithUseGuard();
  config.AddRule(7, Direction::ToCenter, DoorOpen::Any, 0, 9, 0x4)
      .WithUseGuard();
  config.AddRuleFollowing(7, Direction::ToCenter)
      .WithGuard()
      .WithMinGuardHp(guard_health)
      .WithMinKidHp(kid_health);
  config.AddRuleFollowing(7, Direction::ToCenter, DoorOpen::Any, 0, 9, 0x2)
      .WithGuard()
      .WithMinGuardHp(guard_health)
      .WithMinKidHp(kid_health);
  config.AddRuleFollowing(7, Direction::ToLeft, DoorOpen::Any, 0, 9, 0x4)
      .WithGuard()
      .WithMinGuardHp(guard_health);
  config.AddRuleFollowing(7).WithTrob(0x0716).WithMinGuardHp(guard_health);
  config.AddRule(2, Direction::ToLeft, DoorOpen::No, 0, 9, 0x4);
  config.AddRuleFollowing(1, Direction::No, DoorOpen::No, 0, 9, 0x4);
  config.AddRuleFollowing(3, Direction::ToLeft);
  config.AddRule(3, Direction::ToRight, DoorOpen::No, 0, 9, 0x3f)
      .WithTrob(0x031a);
  config.AddRule(3, Direction::ToLeft, DoorOpen::Yes);

  config.search_width = 555555;
  // config.search_width = 1111111;
  // config.search_width = 40000;
  config.novelty_boost = 1.0f;

  config.dup_score = -0.03f;
  config.global_dup_score = 0.0f;
  config.score_decay = 0.3f;
  return config;
}

SearchConfig Level7LongConfig() {
  using Direction = SearchConfig::Direction;
  using DoorOpen = SearchConfig::DoorOpen;
  SearchConfig config;

  // config.run_name = "lvl7";
  config.run_name = "lvl7-nosword-3G";
  config.AddLossRule(7, 0x2);
  config.AddRule(1, Direction::ToRight);
  config.AddRule(2, Direction::ToRight);
  config.AddRule(7, Direction::ToRight);
  config.AddRule(9, Direction::No);
  config.AddRule(23, Direction::ToRight);
  config.AddRule(22, Direction::ToRight);
  config.AddRule(12, Direction::No);
  config.AddRule(24, Direction::No);
  config.AddRule(10, Direction::No);
  config.AddRule(10, Direction::ToLeft, DoorOpen::No, 0, 9, 0x1);
  config.AddRule(9, Direction::No, DoorOpen::No, 5, 9);
  config.AddRule(9, Direction::ToLeft, DoorOpen::No, 0, 9, 0x4);
  config.AddRule(7, Direction::ToLeft, DoorOpen::No, 0, 9, 0x4);
  config.AddRule(2, Direction::ToLeft, DoorOpen::No, 0, 9, 0x4);
  config.AddRule(1, Direction::No, DoorOpen::No, 0, 9, 0x4);
  config.AddRule(3, Direction::ToLeft);
  config.AddRule(3, Direction::ToRight, DoorOpen::No, 0, 9, 0x3f)
      .WithTrob(0x031a);
  config.AddRule(3, Direction::ToLeft, DoorOpen::Yes);

  config.search_width = 100000;
  // config.search_width = 40000;
  config.novelty_boost = 1.5f;

  config.dup_score = -0.05f;
  config.global_dup_score = -0.001f;
  config.score_decay = 0.4f;

  return config;
}

SearchConfig Level10Config() {
  using Direction = SearchConfig::Direction;
  using DoorOpen = SearchConfig::DoorOpen;
  SearchConfig config;

  config.run_name = "lvl10";
  config.AddRule(1, Direction::ToLeft, DoorOpen::No, 0, 9, 0x2);
  config.AddRule(1, Direction::ToRight, DoorOpen::No, 0, 9, 0x4);
  config.AddRule(2, Direction::ToRight, DoorOpen::No, 0, 9, 0x6);
  config.AddRule(7, Direction::ToLeft);
  // config.AddRule(7, Direction::ToLeft, DoorOpen::No, 0, 9, 0x3f, 0x071a);
  // config.AddRule(4, Direction::No, DoorOpen::No, 0, 9, 0x3f, 0x071a);
  config.AddRule(13, Direction::No);
  config.AddRule(2, Direction::ToLeft, DoorOpen::No, 0, 9, 0x1);
  config.AddRule(1, Direction::ToRight, DoorOpen::No, 0, 9, 0x3f)
      .WithTrob(0x0509);
  config.AddRule(5, Direction::ToLeft, DoorOpen::No, 0, 9, 0x1);
  config.AddRule(11, Direction::No);
  config.AddRule(8, Direction::ToLeft, DoorOpen::Any, 0, 9, 0x1);
  config.AddRule(8, Direction::ToCenter, DoorOpen::Yes, 0, 9, 0x6);

  return config;
}

SearchConfig Level10ShortcutConfig() {
  using Direction = SearchConfig::Direction;
  using DoorOpen = SearchConfig::DoorOpen;
  SearchConfig config;

  config.run_name = "lvl10-old";
  config.AddRule(1, Direction::ToRight, DoorOpen::No, 0, 9, 0x3);
  config.AddRule(2, Direction::ToLeft, DoorOpen::No, 0, 9, 0x3);
  config.AddRule(1, Direction::ToRight, DoorOpen::No, 0, 9, 0x3f)
      .WithTrob(0x0509);
  config.AddRule(5, Direction::ToLeft, DoorOpen::No, 0, 9, 0x1);
  config.AddRule(11, Direction::No);
  config.AddRule(8, Direction::ToLeft, DoorOpen::Any, 0, 9, 0x1);
  config.AddRule(8, Direction::ToCenter, DoorOpen::Yes, 0, 9, 0x6);
  // config.search_width = 100000;
  config.search_width = 400000;
  config.novelty_boost = .5f;

  config.dup_score = -0.05f;
  config.global_dup_score = -0.001f;
  config.score_decay = 0.4f;

  return config;
}

SearchConfig Level11Config() {
  using Direction = SearchConfig::Direction;
  using DoorOpen = SearchConfig::DoorOpen;
  SearchConfig config;

  // config.run_name = "old-lvl11";
  config.run_name = "lvl11-nosword-G3";
  config.AddRule(6, Direction::ToRight);
  config.AddRule(9, Direction::ToRight);
  config.AddRule(1, Direction::ToRight);
  config.AddRule(8, Direction::ToRight);
  config.AddRule(15, Direction::No);
  config.AddRule(14, Direction::ToRight);
  config.AddRule(7, Direction::ToRight);
  config.AddRuleFollowing(2, Direction::ToLeft);
  config.AddRuleFollowing(2, Direction::ToRight, DoorOpen::No, 0, 9, 0x3f)
      .WithTrob(0x0211);
  config.AddRule(19, Direction::ToRight);
  config.AddRule(22, Direction::ToRight);
  config.AddRule(24, Direction::ToRight);
  config.AddRule(16, Direction::ToRight);
  config.AddRule(13, Direction::No);
  config.AddRule(13, Direction::ToLeft, DoorOpen::Yes);
  config.AddRule(16, Direction::ToLeft, DoorOpen::Yes);
  config.AddRule(24, Direction::ToLeft, DoorOpen::Yes);

  // config.search_width = 333333;
  config.search_width = 111111;
  // config.search_width = 555555;
  // config.search_width = 1111111;
  // config.novelty_boost = 1.5f;
  config.novelty_boost = 15.5f;

  config.dup_score = -0.05f;
  config.global_dup_score = -0.001f;
  config.score_decay = 0.7f;

  return config;
}

SearchConfig Level12Config() {
  using Direction = SearchConfig::Direction;
  using DoorOpen = SearchConfig::DoorOpen;
  SearchConfig config;

  config.run_name = "lvl12";
  config.AddRule(3, Direction::ToRight, DoorOpen::No, 0, 9, 0x6);
  config.AddRule(4, Direction::ToRight);
  config.AddRule(9, Direction::ToLeft);
  config.AddRule(1, Direction::No);
  config.AddRule(7, Direction::ToRight);
  config.AddRule(8, Direction::ToRight, DoorOpen::No, 0, 9, 0x3);
  config.AddRule(24, Direction::No);
  config.AddRule(14, Direction::ToLeft);
  config.AddRule(12, Direction::ToCenter);
  config.AddRule(19, Direction::ToLeft);
  config.AddRule(15, Direction::ToLeft);
  config.AddRule(2, Direction::ToLeft);
  config.AddRule(13, Direction::ToLeft);
  config.AddRule(23, Direction::ToLeft);

  return config;
}

SearchConfig Level13Config() {
  using Direction = SearchConfig::Direction;
  using DoorOpen = SearchConfig::DoorOpen;
  SearchConfig config;

  config.run_name = "lvl13";
  config.AddRule(13, Direction::ToLeft);
  config.AddRule(23, Direction::ToLeft);
  config.AddRule(16, Direction::ToLeft);
  config.AddRule(4, Direction::ToLeft);
  config.AddRule(3, Direction::ToRight);
  config.AddRule(1, Direction::ToCenter);
  config.AddRule(1, Direction::ToLeft, DoorOpen::Yes);
  config.AddRule(3, Direction::ToLeft, DoorOpen::Yes);

  return config;
}

SearchConfig Level14Config() {
  using Direction = SearchConfig::Direction;
  using DoorOpen = SearchConfig::DoorOpen;
  SearchConfig config;

  config.run_name = "lvl14";
  config.AddRule(4, Direction::ToRight);
  config.AddRule(4, Direction::ToLeft, DoorOpen::No, 0, 9, 0x3f)
      .WithTrob(0x0313);
  config.AddRule(3, Direction::ToLeft);
  config.AddRule(2, Direction::ToLeft);
  config.AddRule(1, Direction::ToLeft);
  config.AddRule(5, Direction::ToLeft);

  return config;
}

SearchConfig Level8Config() {
  using Direction = SearchConfig::Direction;
  using DoorOpen = SearchConfig::DoorOpen;
  SearchConfig config;

  // config.run_name = "lvl8-noseed";
  config.run_name = "lvl8-mouse";
  config.AddLossRule(3, -100, 140, 0, 100);
  // config.AddLossRule(3, 0x2, -100, 120);
  config.AddRule(1, Direction::ToLeft);
  config.AddRule(5, Direction::ToRight).WithUseGuard().WithGuard();  // tmp
  config.AddRuleFollowing(1, Direction::ToRight)
      .WithUseGuard()
      .WithGuard();  // tmp
  config.AddRuleFollowing(2, Direction::ToRight)
      .WithUseGuard()
      .WithGuard();  // tmp
  config.AddRule(2, Direction::No, DoorOpen::Any, 0, 9, 0x4);
  /*config.AddRule(7, Direction::ToCenter);
  config.AddRule(8, Direction::ToRight);
  config.AddRule(6, Direction::ToRight);
  config.AddRule(3, Direction::ToRight, DoorOpen::No, 0, 9, 0x6);*/
  config.AddRule(4, Direction::ToRight, DoorOpen::No, 0, 9, 0x4);
  config.AddRule(16, Direction::ToRight, DoorOpen::No, 0, 9, 0x4);
  config.AddRule(12, Direction::ToRight, DoorOpen::No, 0, 9, 0x4);
  config.AddRule(13, Direction::ToRight, DoorOpen::No, 0, 9, 0x4);
  config.AddRule(22, Direction::ToRight, DoorOpen::No, 0, 9, 0x4);
  config.AddRule(23, Direction::ToRight, DoorOpen::No, 0, 9, 0x4);
  config.AddRule(24, Direction::ToRight, DoorOpen::No, 0, 9, 0x4);
  config.AddRule(18, Direction::ToRight, DoorOpen::No, 0, 5, 0x4);
  config.AddRule(9);
  config.AddRule(18, Direction::No, DoorOpen::No, 6, 9);
  config.AddRule(18, Direction::ToLeft, DoorOpen::No, 0, 9, 0x3);
  config.AddRule2(24, Direction::ToLeft, DoorOpen::No, 0, 9, -100, 120);
  config.AddRule2(23, Direction::ToLeft, DoorOpen::No, 0, 9, -100, 120);
  config.AddRule2(22, Direction::ToLeft, DoorOpen::No, 0, 9, -100, 120);
  config.AddRule2(13, Direction::ToLeft, DoorOpen::No, 0, 9, -100, 120);
  config.AddRule2(12, Direction::ToLeft, DoorOpen::No, 0, 9, -100, 120);
  config.AddRule2(16, Direction::ToLeft, DoorOpen::No, 0, 9, -100, 120);
  config.AddRule2(4, Direction::ToLeft, DoorOpen::No, 0, 9, -100, 120);
  config.AddRule2(3, Direction::ToLeft, DoorOpen::No, 4, 9, -100, 120);
  config.AddRule().WithDoorOpen(DoorOpen::Yes);
  config.AddRule2(3, Direction::ToRight, DoorOpen::Yes, 0, 9, -100, 120);
  config.AddRule2(4, Direction::ToRight, DoorOpen::Yes, 0, 9, -100, 120);
  config.AddRule(16, Direction::ToCenter, DoorOpen::Yes, 0, 9, 0x3);
  config.AddRule(16, Direction::ToLeft, DoorOpen::Yes, 0, 9, 0x4);
  config.AddRule(4, Direction::ToLeft, DoorOpen::Yes, 0, 9, 0x4);
  config.AddRule(3, Direction::ToLeft, DoorOpen::Yes, 0, 9, 0x4);

  // config.search_width = 1111111;
  config.search_width = 111111;
  config.novelty_boost = 1.5f;

  config.dup_score = -0.05f;
  config.global_dup_score = -0.001f;
  config.score_decay = 0.3f;

  return config;
}

// Level 6
/*AddRule(24, Direction::ToLeft);
AddRule(2, Direction::ToLeft);
AddRule(15, Direction::ToLeft);
AddRule(6, Direction::ToLeft);
AddRule(18, Direction::ToLeft);
AddRule(1, Direction::ToLeft); */

// Level 7
/*
AddLossRule(7, 66, 128);
AddRule(1, Direction::ToRight);
AddRule(2, Direction::ToRight);
AddRule(7, Direction::ToRight);
AddRule(9, Direction::No);
AddRule(23, Direction::ToRight);
AddRule(22, Direction::ToRight);
AddRule(12, Direction::No);
AddRule(24, Direction::No);
AddRule(10, Direction::No);
AddRule(10, Direction::ToLeft, DoorOpen::No, 0, 9, 0x1);
AddRule(9, Direction::No, DoorOpen::No, 5, 9);
AddRule(9, Direction::ToLeft, DoorOpen::No, 0, 9, 0x4);
AddRule(7, Direction::ToLeft, DoorOpen::No, 0, 9, 0x4);
AddRule(2, Direction::ToLeft, DoorOpen::No, 0, 9, 0x4);
AddRule(1, Direction::No, DoorOpen::No, 0, 9, 0x4);
AddRule(3, Direction::ToLeft);
AddRule(3, Direction::ToRight, DoorOpen::No, 0, 9, 0x3f, 0x031a);
AddRule(3, Direction::ToLeft, DoorOpen::Yes);
*/

// Level 8
/* AddRule(1, Direction::ToLeft);
AddRule(5, Direction::ToLeft);
AddRule(7, Direction::ToCenter);
AddRule(8, Direction::ToRight);
AddRule(6, Direction::ToRight);
AddRule(3, Direction::ToRight, DoorOpen::No, 0, 9, 0x6);
AddRule(4, Direction::ToRight, DoorOpen::No, 0, 9, 0x4);
AddRule(16, Direction::ToRight, DoorOpen::No, 0, 9, 0x4);
AddRule(12, Direction::ToRight, DoorOpen::No, 0, 9, 0x4);
AddRule(13, Direction::ToRight, DoorOpen::No, 0, 9, 0x4);
AddRule(22, Direction::ToRight, DoorOpen::No, 0, 9, 0x4);
AddRule(23, Direction::ToRight, DoorOpen::No, 0, 9, 0x4);
AddRule(24, Direction::ToRight, DoorOpen::No, 0, 9, 0x4);
AddRule(18, Direction::ToRight, DoorOpen::No, 0, 5, 0x4);
AddRule(9);
AddRule(18, Direction::No, DoorOpen::No, 6, 9);
AddRule(18, Direction::ToLeft, DoorOpen::No, 0, 9, 0x3);
AddRule2(24, Direction::ToLeft, DoorOpen::No, 0, 9, -100, 120);
AddRule2(23, Direction::ToLeft, DoorOpen::No, 0, 9, -100, 120);
AddRule2(22, Direction::ToLeft, DoorOpen::No, 0, 9, -100, 120);
AddRule2(13, Direction::ToLeft, DoorOpen::No, 0, 9, -100, 120);
AddRule2(12, Direction::ToLeft, DoorOpen::No, 0, 9, -100, 120);
AddRule2(16, Direction::ToLeft, DoorOpen::No, 0, 9, -100, 120);
AddRule2(4, Direction::ToLeft, DoorOpen::No, 0, 9, -100, 120);
AddRule2(3, Direction::ToLeft, DoorOpen::No, 4, 9, -100, 120);
AddRule2(3, Direction::ToRight, DoorOpen::Yes, 0, 9, -100, 120);
AddRule2(4, Direction::ToRight, DoorOpen::Yes, 0, 9, -100, 120);
AddRule(16, Direction::ToCenter, DoorOpen::Yes, 0, 9, 0x3);
AddRule(16, Direction::ToLeft, DoorOpen::Yes, 0, 9, 0x4);
AddRule(4, Direction::ToLeft, DoorOpen::Yes, 0, 9, 0x4);
AddRule(3, Direction::ToLeft, DoorOpen::Yes, 0, 9, 0x4); */

// Level 9
/*AddRule(11, Direction::ToLeft);
AddRule(9, Direction::ToLeft);
AddRule(4, Direction::ToLeft);
AddRule(2, Direction::ToRight);
AddRule(5, Direction::ToRight);
AddRule(13, Direction::ToRight);
AddRule(15, Direction::ToCenter, DoorOpen::Any, 0, 7);
AddRule(16, Direction::ToRight);
AddRule(16, Direction::ToRight, DoorOpen::No, 0, 9, 0x1);
AddRule(20, Direction::ToRight);
AddRule(20, Direction::ToLeft, DoorOpen::No, 0, 9, 0x3f, 0x0c13);
AddRule(16, Direction::ToLeft, DoorOpen::No, 0, 9, 0x3f, 0x0c13);
AddRule(12, Direction::ToLeft);
AddRule(18, Direction::ToLeft);
AddRule(17, Direction::ToLeft);
AddRule(19, Direction::ToLeft);
AddRule(22, Direction::ToLeft, DoorOpen::No, 0, 9, 0x3);
AddRule(22, Direction::ToLeft, DoorOpen::No, 0, 9, 0x3f, 0x0a1d);
AddRule(10, Direction::ToLeft);
AddRule(24, Direction::No);
AddRule(1, Direction::ToRight);
AddRule(1, Direction::ToLeft, DoorOpen::No, 0, 9, 0x3f, 0x010b);
AddRule(1, Direction::ToRight, DoorOpen::No, 0, 9, 0x3f, 0x0105);
AddRule(23, Direction::ToRight);
AddRule(2, Direction::ToRight, DoorOpen::No, 0, 9, 0x1);
AddRule(5, Direction::No, DoorOpen::No, 0, 4, 0x1);
AddRule(5, Direction::ToCenter, DoorOpen::Yes);*/

SearchConfig DefaultConfig() { return Level10ShortcutConfig(); }
