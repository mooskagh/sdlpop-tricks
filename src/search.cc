#include "search.h"

#include <bits/stdint-uintn.h>

#include <algorithm>
#include <fstream>
#include <iostream>

#include "metrohash64.h"
#include "serializer.h"
#include "state.h"

namespace {
#define DUMP_SNAPS
constexpr uint64_t kInterestingHash = 11276976365734884338ULL;
constexpr float kTaintedBoost = 0.0f;

const Move kMoveOrder[] = {
    Move::Nothing, Move::U,  Move::L, Move::R,  Move::LU,
    Move::Shift,   Move::RU, Move::D, Move::SR, Move::SL,
    Move::RD,      Move::LD, Move::SU
    // Move::SRU,      //
    // Move::SLU,      //
    // Move::SD,       //
    // Move::SRD,      //
    // Move::SLD,      //
};

#ifdef DEBUG_TAINT

const Move kDebugMoves[] = {};
#endif

std::string GetTimeStr() {
  time_t rawtime;
  struct tm* timeinfo;
  char buffer[80];

  time(&rawtime);
  timeinfo = localtime(&rawtime);

  strftime(buffer, sizeof(buffer), "%Y%m%d-%H%M", timeinfo);
  return buffer;
}

}  // namespace

std::string MoveToStr(Move m) {
  switch (m) {
    case Move::RU:
      return "RU";
    case Move::LU:
      return "LU";
    case Move::R:
      return "R";
    case Move::L:
      return "L";
    case Move::U:
      return "U";
    case Move::SR:
      return "SR";
    case Move::SL:
      return "SL";
    case Move::Shift:
      return "S";
    case Move::D:
      return "D";
    case Move::Nothing:
      return ".";
    case Move::RD:
      return "RD";
    case Move::LD:
      return "LD";
    case Move::SU:
      return "SU";
    case Move::SRU:
      return "SRU";
    case Move::SLU:
      return "SLU";
    case Move::SD:
      return "SD";
    case Move::SRD:
      return "SRD";
    case Move::SLD:
      return "SLD";
    case Move::Restart:
      return "a";
    case Move::SoundOff:
      return "e";
  }  // namespace
  return "????";
}

std::string MovesToStr(const std::string& moves) {
  std::string result;
  for (const auto& x : moves) {
    if (!result.empty()) result += ' ';
    result += MoveToStr(Move(x));
  }
  return result;
}

extern "C" {
extern int interesting;
}

IterationResult Search::DoOneFrame(int frame) {
  const int kShowEvery = std::clamp(config_.search_width / 10, 8000, 30000);
  const int kShowSkip = std::max(2, 35000 / kShowEvery);
  // const int kShowEvery = 5000;
  // const int kShowSkip = 1;

  IterationResult result;
  State state;

  next_frames_.clear();
  kid_counts_.clear();

  int show_skip = kShowSkip;

  for (size_t idx = 0; idx < cur_frames_.size(); ++idx) {
    const auto& cur = cur_frames_[idx];

#ifdef DEBUG_TAINT
    if (cur.tainted) {
      std::cout << "Tainted move at rank " << idx << ".";
      if (size_t(frame) < sizeof(kDebugMoves)) {
        std::cout << " Expected move is " << int(kDebugMoves[frame]);
      }
      std::cout << "\n";
    }
#endif

    for (const auto& move : kMoveOrder) {
#ifdef DEBUG_TAINT
      const bool tainted = cur.tainted && size_t(frame) < sizeof(kDebugMoves) &&
                           kDebugMoves[frame] == move;
#endif
      state.LoadBase(base_state_);
      state.LoadFrame(cur.snapshot);
      prince_.PrepareGameAfterLoad();

      if (move == Move(0) && idx == 0) {
        std::cout << "Kid.x=" << int(Kid.x) << " Kid.y=" << int(Kid.y)
                  << " Guard.x=" << int(Guard.x) << " Guard.y=" << int(Guard.y)
                  << " Guard.alive=" << int(Guard.alive)
                  << " Guard.room=" << int(Guard.room) << " guardhp_curr"
                  << int(guardhp_curr) << " room=" << drawn_room;
        std::cout << "\nBest frame hash " << state.ComputeHash()
                  << " score: " << cur.score << "\n";

        prince_.Draw();
#ifdef DUMP_SNAPS
        const std::string filename =
            config_.run_name + "_" + GetTimeStr() + "_" + std::to_string(frame);
        state.Quicksave(kRootDir + "saves/qsavs/" + filename + ".sav");
        std::ofstream fo(kRootDir + "saves/snaps/" + filename + ".txt",
                         std::ios_base::app);
        for (const auto& x : cur.moves) {
          fo << MoveToStr(Move(x)) << ' ';
        }

#endif
      } else if (move == Move(0) && int(idx) % kShowEvery == kShowEvery / 2) {
        if (show_skip-- <= 0) prince_.Draw();
      }

      prince_.AdvanceOneFrame(int(move));

      /* if (interesting) {
        interesting = 0;
        const std::string filename = config_.run_name + "_" + GetTimeStr() +
                                     "_F" + std::to_string(frame) + "_I" +
                                     std::to_string(idx) + "_M" +
                                     std::to_string(int(move));
        std::ofstream fo(kRootDir + "saves/interesting/" + filename,
                         std::ios_base::app);
        for (const auto& x : cur.moves) {
          fo << MoveToStr(Move(x)) << ' ';
        }
        fo << MoveToStr(Move(move)) << "\n";
      } */

      if (scorer_.IsLoss()) {
        ++result.losses;
#ifdef DEBUG_TAINT
        if (tainted) std::cout << "Tainted move is a loss!\n";
#endif
        continue;
      }
      if (scorer_.IsWin()) {
        result.win = true;
        std::cout << "\n\n\n!!!! WIN !!!\n";
        std::cout << "Lowest rank: " << cur.lowest_rank << "\n";
        for (const auto& x : cur.moves) {
          std::cout << MoveToStr(Move(x)) << ' ';
        }
        std::cout << MoveToStr(Move(move)) << "\n\n\n";
        std::ofstream fo(kRootDir + "saves/win/" + config_.run_name + "_" +
                             GetTimeStr() + ".txt",
                         std::ios_base::app);
        for (const auto& x : cur.moves) {
          fo << MoveToStr(Move(x)) << ' ';
        }
        fo << MoveToStr(Move(move)) << "\n";
        state.Quicksave(kRootDir + "saves/winstate/" + config_.run_name + "_" +
                        GetTimeStr() + ".sav");
        return result;
      }
      const auto hash = state.ComputeHash();
#ifdef DEBUG_TAINT
      if (tainted) {
        std::cout << "Tainted move hash = [" << hash
                  << "], seed=" << random_seed << "\n";
      }
#endif

      if (hash == kInterestingHash) {
        const std::string filename =
            config_.run_name + "_H" + std::to_string(hash) + "_F" +
            std::to_string(frame) + "_M" + std::to_string(int(move)) + "_C" +
            std::to_string(idx) + "_" + GetTimeStr();
        std::cout << "Storing kInterestingHash to " << filename;
        std::cout << " Moves: " << MovesToStr(cur.moves);
        std::cout << " (+" << MoveToStr(move);
        std::cout << ")\n";
        state.Quicksave(kRootDir + "saves/hashes/" + filename);
      }

      if (!hashes_.insert(hash).second) {
        ++result.hash_collisions;
#ifdef DEBUG_TAINT
        if (tainted) {
          std::cout << "Tainted move is a collision!\n";
        }
#endif
        continue;
      }

#ifdef DEBUG_TAINT
      if (tainted) {
        const std::string filename =
            config_.run_name + "_H" + std::to_string(hash) + "_F" +
            std::to_string(frame) + "_M" + std::to_string(int(move)) + "_C" +
            std::to_string(idx) + "_" + GetTimeStr();
        state.Quicksave(kRootDir + "saves/taintstates/" + filename);
      }
#endif

      auto kid_hash = state.KidHash();
      int kid_dups = kid_counts_[kid_hash]++;
      int global_kid_dups = global_kid_counts_[kid_hash]++;
      auto static_score = scorer_.Score(cur.static_score);
      float dynamic_score = config_.dup_score * kid_dups +
                            config_.global_dup_score * global_kid_dups;
      if (global_kid_dups == 0.0f) {
        dynamic_score += config_.novelty_boost;
        ++result.novel_positions;
      }
      float score = static_score.score + dynamic_score;

      if (kid_dups > result.largest_kid) result.largest_kid = kid_dups;
      if (global_kid_dups > result.global_largest_kid) {
        result.global_largest_kid = global_kid_dups;
      }

      if (score < result.worst_score) result.worst_score = score;
      ++result.candidates;

#ifdef DEBUG_TAINT
      if (tainted) score += kTaintedBoost;
#endif

      if (int(next_frames_.size()) == config_.search_width) {
        if (score > next_frames_.back().score) {
#ifdef DEBUG_TAINT
          if (next_frames_.back().tainted) {
            std::cout << "Evicted tainted move with score "
                      << next_frames_.back().score;
          }
#endif
          next_frames_.pop_back();
          next_frames_.emplace_back(static_score, dynamic_score, score,
                                    std::max(cur.lowest_rank, int(idx)),
                                    state.SaveFrame(), cur.moves, char(move));
#ifdef DEBUG_TAINT
          if (tainted) next_frames_.back().tainted = true;
#endif
          std::push_heap(next_frames_.begin(), next_frames_.end());
          std::pop_heap(next_frames_.begin(), next_frames_.end());
        } else {
#ifdef DEBUG_TAINT
          if (tainted) {
            std::cout << "Had score " << score
                      << " which is less than worst score "
                      << next_frames_.back().score << "\n";
            // scorer_.DebugPrint(int(move));
          }
#endif
        }
      } else {
        next_frames_.emplace_back(static_score, dynamic_score, score,
                                  std::max(cur.lowest_rank, int(idx)),
                                  state.SaveFrame(), cur.moves, char(move));
#ifdef DEBUG_TAINT
        if (tainted) next_frames_.back().tainted = true;
#endif
        if (int(next_frames_.size()) == config_.search_width) {
          std::make_heap(next_frames_.begin(), next_frames_.end());
          std::pop_heap(next_frames_.begin(), next_frames_.end());
        }
      }
    }
  }
  cur_frames_.clear();
  std::sort(next_frames_.begin(), next_frames_.end(),
            [](const auto& a, const auto& b) { return a.score > b.score; });

  std::swap(cur_frames_, next_frames_);
  return result;
}

Search::Search(int argc, char* argv[], const SearchConfig& config)
    : prince_(argc, argv), scorer_(config), config_(config) {
  State state;
  state.Quickload(kRootDir + "saves/savs/" + config_.run_name + ".sav");
  base_state_ = state.SaveBase();
  hashes_.insert(state.ComputeHash());
  cur_frames_.emplace_back(ScoreResult{0.0f, 0.0f, 0}, 0.0, -1000, 0,
                           state.SaveFrame(), "", '\0');
  cur_frames_.back().moves.clear();
#ifdef DEBUG_TAINT
  cur_frames_.back().tainted = true;
#endif
}

void Search::Run(int limit) {
  for (int i = 0; i < limit; ++i) {
    if (cur_frames_.empty()) break;

    std::cout << "== Starting frame " << i << std::endl;
    auto res = DoOneFrame(i);
    if (!cur_frames_.empty()) {
      std::cout << "best_score:" << cur_frames_.front().score
                << " best_pos_score:"
                << cur_frames_.front().static_score.position_score
                << " best_static_score:"
                << cur_frames_.front().static_score.score
                << " best_dynamic_score:" << cur_frames_.front().dynamic_score
                << " best_rule:" << cur_frames_.front().static_score.best_rule
                << " worst_fit_score:" << cur_frames_.back().score
                << " best_lowest_rank:" << cur_frames_.front().lowest_rank
                << "\n";
    }
    std::cout << "worst_score:" << res.worst_score
              << " hash_collisions:" << res.hash_collisions
              << " losses:" << res.losses << " candidates:" << res.candidates
              << "\nhash_size:" << hashes_.size()
              << " action:" << int(Kid.action) << " room:" << int(drawn_room)
              << " Kid.y:" << int(Kid.y) << " Kid.x:" << int(Kid.x)
              << " final_size:" << cur_frames_.size()
              << " row:" << ((int(Kid.y) + 60) / 63 - 1)
              << " uniq_kids:" << kid_counts_.size()
              << " largest_kid:" << res.largest_kid
              << " g_uniq_kids:" << global_kid_counts_.size()
              << " g_largest_kid:" << res.global_largest_kid
              << " novel_pos:" << res.novel_positions;
    std::cout << std::endl;
    if (res.win) {
      prince_.Draw();
      std::cout << "Exiting due to win\n";
      getchar();
      return;
    }
    // getchar();
  }
}

void Search::DumpState(const std::string& filename, int only_room) {
  Writer wr(filename);
  wr.Write(base_state_);
  for (const auto& x : cur_frames_) {
    if (only_room == -1 || only_room == drawn_room) {
      wr.Write(x.moves);
      wr.Write(x.snapshot);
    }
  }
}
