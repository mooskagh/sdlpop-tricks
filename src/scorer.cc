#include "scorer.h"

#include <iostream>
#include <vector>

#include "sdlpop/common.h"

namespace {

float GetTrobsScore() {
  for (int i = 0; i < trobs_count; ++i) {
    const auto& trob = trobs[i];
    const auto idx = (trob.room - 1) * 30 + trob.tilepos;
    const auto type = level.fg[idx] & 0x1f;
    if (type == tiles_16_level_door_left) return 1.0f;
  }
  return 0.0f;
}

}  // namespace

void Scorer::PositionDebugPrint() const {
  std::cout << "Kid.x = " << int(Kid.x) << "\n";
  std::cout << "Kid.y = " << int(Kid.y) << "\n";
  int row = (int(Kid.y) + 60) / 63 - 1;
  int col = (int(Kid.x) - 7 - 58);
  if (col >= 0) {
    col /= 14;
  } else {
    col = col / 14 - 1;
  }
  std::cout << "row = " << row << "\n";
  std::cout << "col = " << col << "\n";
  std::cout << "room = " << drawn_room << "\n";
  bool door_open = leveldoor_open;

  if (!door_open) {
    for (int i = 0; i < trobs_count; ++i) {
      const auto& trob = trobs[i];
      const auto idx = (trob.room - 1) * 30 + trob.tilepos;
      const auto type = level.fg[idx] & 0x1f;
      if (type == tiles_16_level_door_left) {
        door_open = true;
        break;
      }
    }
  }
  std::cout << "door_open = " << door_open << "\n";
}

std::pair<float, int> Scorer::PositionScore(
    const ScoreResult& prev_score) const {
  using DoorOpen = SearchConfig::DoorOpen;
  using Direction = SearchConfig::Direction;

  constexpr float kUnknownRoom = -0.5f;

  int kid_y = int(Kid.y);
  int col = (int(Kid.x) - 7 - 58);
  if (col >= 0) {
    col /= 14;
  } else {
    col = col / 14 - 1;
  }

  int guard_y = int(Guard.y);
  int guard_col = (int(Guard.x) - 7 - 58);
  if (guard_col >= 0) {
    guard_col /= 14;
  } else {
    guard_col = guard_col / 14 - 1;
  }

  bool door_open = leveldoor_open;

  if (!door_open) {
    for (int i = 0; i < trobs_count; ++i) {
      const auto& trob = trobs[i];
      const auto idx = (trob.room - 1) * 30 + trob.tilepos;
      const auto type = level.fg[idx] & 0x1f;
      if (type == tiles_16_level_door_left) {
        door_open = true;
        break;
      }
    }
  }

  for (int i = int(config_.rules.size()) - 1; i >= 0; --i) {
    const auto& rule = config_.rules[i];
    if (rule.room && *rule.room != drawn_room) continue;
    if (door_open && rule.door_open == DoorOpen::No) continue;
    if (!door_open && rule.door_open == DoorOpen::Yes) continue;
    if (rule.must_have_guard && drawn_room != Guard.room) continue;
    if (hitp_curr < rule.min_kid_hp) continue;

    if (drawn_room == Guard.room) {
      if (guardhp_curr < rule.min_guard_hp) continue;
    }

    if (rule.use_guard_coords) {
      if (!Guard.alive) continue;
      if (rule.x_from && *rule.x_from != 0 && guard_col < *rule.x_from)
        continue;
      if (rule.x_to && *rule.x_to != 9 && guard_col > *rule.x_to) continue;
      if (rule.y_from && rule.y_to &&
          (guard_y < *rule.y_from || guard_y > *rule.y_to)) {
        continue;
      }
    } else {
      if (rule.x_from && *rule.x_from != 0 && col < *rule.x_from) continue;
      if (rule.x_to && *rule.x_to != 9 && col > *rule.x_to) continue;
      if (rule.y_from && rule.y_to &&
          (kid_y < *rule.y_from || kid_y > *rule.y_to)) {
        continue;
      }
    }

    if (prev_score.best_rule < rule.min_prev_position) continue;
    if (rule.trob) {
      bool trob_found = false;
      for (int i = 0; i < trobs_count; ++i) {
        const auto& trob = trobs[i];
        if ((trob.room << 8) + trob.tilepos == *rule.trob) {
          trob_found = true;
          break;
        }
      }
      if (!trob_found) continue;
    }

    float base_score = 0.5f + i;
    if (rule.direction == Direction::No) return {base_score, i + 1};

    float theta = 0.5f;
    if (rule.x_from && rule.x_to) {
      int x = rule.use_guard_coords ? Guard.x : Kid.x;
      theta = (((x - 7 - 58) / 14.0) - *rule.x_from) /
              (*rule.x_to + 1 - *rule.x_from);
    }

    if (rule.direction == Direction::ToRight) theta = (theta - 0.5);
    if (rule.direction == Direction::ToLeft) theta = (0.5 - theta);
    if (rule.direction == Direction::ToCenter)
      theta = 1 - 2 * std::abs(theta - 0.5);
    return {base_score + 0.8f * theta, i + 1};
  }

  return {kUnknownRoom, 0};
}

ScoreResult Scorer::Score(const ScoreResult& prev_score) const {
  float score = 0.0f;

  if (leveldoor_open != 0) score += 2.0f;
  auto [pos_score, best_rule] = PositionScore(prev_score);
  score += pos_score * 1.0f;
  score += GetTrobsScore() * 1.0f;
  /*
  if (Kid.frame >= frame_135_climbing_1 && Kid.frame <= frame_149_climbing_15) {
    score += 0.2f;
  }
  if (Kid.frame >= frame_121_stepping_1 && Kid.frame <= frame_132_stepping_12) {
    score += 0.05f;
  }
  if (Kid.frame >= frame_16_standing_jump_1 &&
      Kid.frame <= frame_33_standing_jump_18) {
    score += 0.0f;
  }
  if (Kid.frame >= frame_67_start_jump_up_1 && Kid.frame <= frame_80_jumphang) {
    score += 0.2f;
  }
  */

  score = std::max(score, prev_score.score - config_.score_decay);

  return {score, pos_score, best_rule};
}

bool Scorer::IsLoss() const {
  return Kid.alive == 0 || Kid.frame == frame_185_dead ||
         Kid.frame == frame_177_spiked || Kid.frame == frame_178_chomped ||
         Kid.frame == frame_179_collapse_1 || Char.fall_y >= 33 ||
         IsCustomLoss();
}
bool Scorer::IsWin() const {
  return next_level != current_level || Kid.frame == frame_217_exit_stairs_1;
}

bool Scorer::IsCustomLoss() const {
  for (const auto& x : config_.loss_rules) {
    if (x.room != drawn_room) continue;
    if (Kid.x < x.from_x) continue;
    if (Kid.x > x.to_x) continue;
    if (Kid.y < x.from_y) continue;
    if (Kid.y > x.to_y) continue;
    return true;
  }
  return false;
}
