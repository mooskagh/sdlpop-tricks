#include <iostream>

#include "configs.h"
#include "search.h"
#include "state.h"

int main(int argc, char *argv[]) {
  Search search(argc, argv, DefaultConfig());
  search.Run(12 * 60 * 30);
  // search.Run(23);
  // search.Run(11);
  return 0;
}
