#include <iostream>

#include "configs.h"
#include "search.h"
#include "state.h"

namespace {

std::string MoveToStr(Move m) {
  switch (m) {
    case Move::RU:
      return "RU";
    case Move::LU:
      return "LU";
    case Move::R:
      return "R";
    case Move::L:
      return "L";
    case Move::U:
      return "U";
    case Move::SR:
      return "SR";
    case Move::SL:
      return "SL";
    case Move::Shift:
      return "S";
    case Move::D:
      return "D";
    case Move::Nothing:
      return ".";
    case Move::RD:
      return "RD";
    case Move::LD:
      return "LD";
    case Move::SU:
      return "SU";
    case Move::SRU:
      return "SRU";
    case Move::SLU:
      return "SLU";
    case Move::SD:
      return "SD";
    case Move::SRD:
      return "SRD";
    case Move::SLD:
      return "SLD";
    case Move::Restart:
      return "a";
    case Move::SoundOff:
      return "e";
  }  // namespace
  return "????";
}

const Move kDebugMoves[] = {
    Move::Nothing, Move::Nothing, Move::Nothing, Move::L,       Move::Nothing,
    Move::Nothing, Move::Nothing, Move::Nothing, Move::Nothing, Move::Nothing,
    Move::LU,      Move::Nothing, Move::Nothing, Move::Nothing, Move::Nothing,
    Move::Nothing, Move::Nothing, Move::Nothing, Move::Nothing, Move::Nothing,
    Move::Nothing, Move::Nothing, Move::LU,      Move::Nothing, Move::Nothing,
    Move::Nothing, Move::Nothing, Move::Nothing, Move::Nothing, Move::Nothing,
    Move::Nothing, Move::Nothing, Move::Nothing, Move::Nothing, Move::L,
    Move::Nothing, Move::Nothing, Move::Nothing, Move::L,       Move::Nothing,
    Move::Nothing, Move::R,       Move::Nothing, Move::Nothing, Move::Nothing,
    Move::Nothing, Move::Nothing, Move::Nothing, Move::Nothing, Move::Nothing,
    Move::Nothing, Move::Nothing, Move::Nothing, Move::D,       Move::Nothing,
    Move::Nothing, Move::Nothing, Move::Nothing, Move::Nothing, Move::L,
    Move::Nothing, Move::Nothing, Move::Nothing, Move::Nothing, Move::Nothing,
    Move::Nothing, Move::L,       Move::Nothing, Move::Nothing, Move::Nothing,
    Move::L,       Move::Nothing, Move::U,       Move::Nothing, Move::Nothing,
    Move::Nothing, Move::Nothing, Move::Nothing, Move::Nothing, Move::Nothing,
    Move::Nothing, Move::Nothing, Move::Nothing, Move::Nothing, Move::Nothing,
    Move::Nothing, Move::Nothing, Move::Nothing, Move::Nothing, Move::Nothing,
    Move::U,       Move::Nothing, Move::Nothing, Move::Nothing, Move::Nothing,
    Move::Nothing, Move::Nothing, Move::Nothing, Move::Nothing, Move::Nothing,
    Move::Nothing, Move::Nothing, Move::Nothing, Move::Nothing, Move::Nothing,
    Move::U,       Move::Nothing, Move::Nothing, Move::Nothing, Move::Nothing,
    Move::Nothing, Move::Nothing, Move::Nothing, Move::Nothing, Move::Nothing,
    Move::Nothing, Move::Nothing, Move::Nothing, Move::Nothing, Move::Nothing,
    Move::Nothing, Move::Nothing, Move::Nothing, Move::L,       Move::Nothing,
    Move::Nothing, Move::Nothing, Move::Nothing, Move::Nothing, Move::Nothing,
    Move::L,       Move::Nothing, Move::Nothing, Move::Nothing, Move::L,
    Move::Nothing, Move::Nothing, Move::Nothing, Move::L,       Move::Nothing,
    Move::Nothing, Move::Nothing, Move::L,       Move::Nothing, Move::Nothing,
    Move::Nothing, Move::L,       Move::Nothing, Move::U,       Move::Nothing,
    Move::Nothing, Move::Nothing, Move::Nothing, Move::Nothing, Move::Nothing,
    Move::Nothing, Move::Nothing, Move::Nothing, Move::Nothing, Move::Nothing,
    Move::L,       Move::Nothing, Move::Nothing, Move::Nothing, Move::U,
    Move::Nothing, Move::Nothing, Move::Nothing, Move::Nothing, Move::Nothing,
    Move::Nothing, Move::Nothing, Move::Nothing, Move::Nothing, Move::Nothing,
    Move::Nothing, Move::Nothing, Move::Nothing, Move::Nothing, Move::U,
    Move::Nothing, Move::Nothing, Move::Nothing, Move::Nothing, Move::Nothing,
    Move::Nothing, Move::Nothing, Move::Nothing, Move::Nothing, Move::Nothing,
    Move::Nothing, Move::Nothing, Move::Nothing, Move::Nothing, Move::Nothing,
    Move::Nothing, Move::Nothing, Move::U,       Move::Nothing, Move::Nothing,
    Move::Nothing, Move::Nothing, Move::Nothing, Move::Nothing, Move::Nothing,
    Move::Nothing, Move::Nothing, Move::Nothing, Move::Nothing, Move::Nothing,
    Move::Nothing, Move::Nothing, Move::U,       Move::Nothing, Move::Nothing,
    Move::Nothing, Move::Nothing, Move::Nothing, Move::Nothing, Move::Nothing,
    Move::Nothing, Move::Nothing, Move::Nothing, Move::Nothing, Move::Nothing,
    Move::Nothing, Move::Nothing, Move::Nothing, Move::Nothing, Move::Nothing,
    Move::L,       Move::Nothing, Move::Nothing, Move::Nothing, Move::Nothing,
    Move::Nothing, Move::Nothing, Move::L,       Move::U,       Move::Nothing,
    Move::Nothing, Move::Nothing, Move::Nothing, Move::Nothing, Move::Nothing,
    Move::Nothing, Move::Nothing, Move::Nothing, Move::Nothing, Move::Nothing,
    Move::Nothing, Move::Nothing, Move::Nothing, Move::Nothing, Move::Nothing,
    Move::Nothing, Move::Nothing, Move::Nothing, Move::Nothing, Move::Nothing,
    Move::Nothing, Move::L,       Move::Nothing, Move::U,       Move::Nothing,
    Move::Nothing, Move::Nothing, Move::L,       Move::Nothing, Move::Nothing,
    Move::L,       Move::Nothing, Move::Nothing, Move::Nothing, Move::Nothing,
    Move::L,       Move::Nothing, Move::Nothing, Move::Nothing, Move::Nothing,
    Move::Nothing, Move::Nothing, Move::L,       Move::Nothing, Move::Nothing,
    Move::D,       Move::Nothing, Move::Nothing, Move::Nothing, Move::Nothing,
    Move::Nothing, Move::L,       Move::Nothing, Move::Nothing, Move::Nothing,
    Move::Nothing, Move::Nothing, Move::U,       Move::Nothing, Move::Nothing,
    Move::Nothing, Move::Nothing, Move::Nothing, Move::Nothing, Move::Nothing,
    Move::Nothing, Move::Nothing, Move::Nothing, Move::Nothing, Move::Nothing,
    Move::Nothing, Move::Nothing, Move::U,       Move::Nothing, Move::Nothing,
    Move::Nothing, Move::Nothing, Move::Nothing, Move::Nothing, Move::Nothing,
    Move::Nothing, Move::Nothing, Move::Nothing, Move::Nothing, Move::Nothing,
    Move::Nothing, Move::Nothing, Move::Nothing, Move::Nothing, Move::Nothing,
    Move::L,       Move::Nothing, Move::Nothing, Move::Nothing, Move::Nothing,
    Move::Nothing, Move::Nothing, Move::Nothing, Move::Nothing, Move::Nothing,
    Move::Nothing, Move::Nothing, Move::Nothing, Move::Nothing, Move::Nothing,
    Move::L,       Move::Nothing, Move::Nothing, Move::Nothing, Move::Nothing,
    Move::Nothing, Move::Nothing, Move::L,       Move::Nothing, Move::Nothing,
    Move::Nothing, Move::L,       Move::Nothing, Move::Nothing, Move::U,
    Move::Nothing, Move::Nothing, Move::Nothing, Move::Nothing, Move::Nothing,
    Move::Nothing, Move::Nothing, Move::Nothing, Move::Nothing, Move::Nothing,
    Move::Nothing, Move::L,       Move::Nothing, Move::Nothing, Move::U,
    Move::Nothing, Move::Nothing, Move::Nothing, Move::Nothing, Move::Nothing,
    Move::Nothing, Move::Nothing, Move::Nothing, Move::Nothing, Move::Nothing,
    Move::Nothing, Move::Nothing, Move::Nothing, Move::Shift,   Move::Nothing,
    Move::Nothing, Move::Nothing, Move::Nothing, Move::Nothing, Move::Nothing,
    Move::Nothing, Move::Nothing, Move::Nothing, Move::Nothing, Move::Nothing,
    Move::Nothing, Move::Nothing, Move::Nothing, Move::Nothing, Move::Nothing,
    Move::Nothing, Move::Nothing, Move::Nothing, Move::L,       Move::Nothing,
    Move::Nothing, Move::Nothing, Move::L,       Move::Nothing, Move::Nothing,
    Move::D,       Move::Nothing, Move::Nothing, Move::Nothing, Move::Nothing,
    Move::Nothing, Move::L,       Move::Nothing, Move::Nothing, Move::Nothing,
    Move::L,       Move::Nothing, Move::Nothing, Move::Nothing, Move::Nothing,
    Move::Nothing, Move::Nothing, Move::LU,      Move::Nothing, Move::Nothing,
    Move::Nothing, Move::Nothing, Move::Nothing, Move::Nothing, Move::Nothing,
    Move::Nothing, Move::Nothing, Move::Nothing, Move::Nothing, Move::LU,
    Move::Nothing, Move::Nothing, Move::Nothing, Move::Nothing, Move::Nothing,
    Move::Nothing, Move::Nothing, Move::Nothing, Move::Nothing, Move::Nothing,
    Move::Nothing, Move::Nothing, Move::U,       Move::Nothing, Move::Nothing,
    Move::Nothing, Move::Nothing, Move::Nothing, Move::Nothing, Move::Nothing,
    Move::Nothing, Move::Nothing, Move::Nothing, Move::Nothing, Move::Nothing,
    Move::Nothing, Move::Nothing, Move::U,       Move::Nothing, Move::Nothing,
    Move::Nothing, Move::Nothing, Move::Nothing, Move::Nothing, Move::Nothing,
    Move::Nothing, Move::Nothing, Move::Nothing, Move::Nothing, Move::Nothing,
    Move::Nothing, Move::Nothing, Move::Nothing, Move::Nothing, Move::Nothing,
    Move::L,       Move::Nothing, Move::Nothing, Move::Nothing, Move::Nothing,
    Move::Nothing, Move::Nothing, Move::L,       Move::U,       Move::Nothing,
    Move::Nothing, Move::Nothing, Move::Nothing, Move::Nothing, Move::Nothing,
    Move::Nothing, Move::Nothing, Move::Nothing, Move::Nothing, Move::Nothing,
    Move::Nothing, Move::Nothing, Move::Shift,   Move::Nothing, Move::Nothing,
    Move::Nothing, Move::Nothing, Move::Nothing, Move::Nothing, Move::Nothing,
    Move::Nothing, Move::Nothing, Move::Nothing, Move::Nothing, Move::Nothing,
    Move::Nothing, Move::Nothing, Move::Nothing, Move::Nothing, Move::Nothing,
    Move::Nothing, Move::Nothing, Move::Nothing, Move::L,       Move::Nothing,
    Move::Nothing, Move::L,       Move::Nothing, Move::Nothing, Move::L,
    Move::Nothing, Move::Nothing, Move::L,       Move::Nothing, Move::Nothing,
    Move::L,       Move::Nothing, Move::Nothing, Move::Nothing, Move::L,
    Move::Nothing, Move::Nothing, Move::D,       Move::Nothing, Move::Nothing,
    Move::Nothing, Move::Nothing, Move::Nothing, Move::L,       Move::Nothing,
    Move::Nothing, Move::Nothing, Move::L,       Move::Nothing, Move::Nothing,
    Move::Nothing, Move::Nothing, Move::Nothing, Move::Nothing, Move::LU,
    Move::Nothing, Move::Nothing, Move::Nothing, Move::Nothing, Move::Nothing,
    Move::Nothing, Move::Nothing, Move::Nothing, Move::Nothing, Move::Nothing,
    Move::Nothing, Move::LU,      Move::Nothing, Move::Nothing, Move::Nothing,
    Move::Nothing, Move::Nothing, Move::Nothing, Move::Nothing, Move::Nothing,
    Move::Nothing, Move::Nothing, Move::Nothing, Move::LU,      Move::Nothing,
    Move::Nothing, Move::Nothing, Move::Nothing, Move::Nothing, Move::Nothing,
    Move::Nothing, Move::Nothing, Move::Nothing, Move::Nothing, Move::Nothing,
    Move::LU,      Move::Nothing, Move::Nothing, Move::Nothing, Move::Nothing,
    Move::Nothing, Move::Nothing, Move::Nothing, Move::Nothing, Move::Nothing,
    Move::Nothing, Move::Nothing, Move::L,       Move::U,       Move::Nothing,
    Move::Nothing, Move::Nothing, Move::Nothing, Move::Nothing, Move::Nothing,
    Move::Nothing, Move::Nothing, Move::Nothing, Move::Nothing, Move::Nothing,
    Move::LU,      Move::Nothing, Move::Nothing, Move::Nothing, Move::Nothing,
    Move::Nothing, Move::Nothing, Move::Nothing, Move::Nothing, Move::Nothing,
    Move::Nothing, Move::Nothing, Move::Nothing, Move::U,       Move::Nothing,
    Move::Nothing, Move::Nothing, Move::Nothing, Move::Nothing, Move::Nothing,
    Move::Nothing, Move::Nothing, Move::Nothing, Move::Nothing, Move::Nothing,
    Move::Nothing, Move::Nothing, Move::Nothing, Move::U,       Move::Nothing,
    Move::Nothing, Move::Nothing, Move::Nothing, Move::Nothing, Move::Nothing,
    Move::Nothing, Move::Nothing, Move::Nothing, Move::Nothing, Move::Nothing,
    Move::Nothing, Move::Nothing, Move::Nothing, Move::Nothing, Move::Nothing,
    Move::Nothing, Move::L,       Move::Nothing, Move::Nothing, Move::Nothing,
    Move::Nothing, Move::Nothing, Move::Nothing, Move::LU,      Move::Nothing,
    Move::Nothing, Move::Nothing, Move::Nothing, Move::Nothing, Move::Nothing,
    Move::Nothing, Move::Nothing, Move::Nothing, Move::Nothing, Move::Nothing,
    Move::Nothing, Move::Nothing, Move::Nothing, Move::U,       Move::Nothing,
    Move::Nothing, Move::Nothing, Move::Nothing, Move::Nothing, Move::Nothing,
    Move::Nothing, Move::Nothing, Move::Nothing, Move::Nothing, Move::Nothing,
    Move::Nothing, Move::Nothing, Move::Nothing, Move::Nothing, Move::Nothing,
    Move::Nothing, Move::Nothing, Move::L,       Move::Nothing, Move::Nothing,
    Move::L,       Move::Nothing, Move::Nothing, Move::Nothing, Move::Nothing,
    Move::U,       Move::Nothing, Move::Nothing, Move::Nothing, Move::Nothing,
    Move::Nothing, Move::Nothing, Move::Nothing, Move::Nothing, Move::Nothing,
    Move::Nothing, Move::Nothing, Move::Nothing, Move::Nothing, Move::Nothing,
    Move::U,       Move::Nothing, Move::Nothing, Move::Nothing, Move::Nothing,
    Move::Nothing, Move::Nothing, Move::Nothing, Move::Nothing, Move::Nothing,
    Move::Nothing, Move::Nothing, Move::Nothing, Move::Nothing, Move::Nothing,
    Move::Nothing, Move::Nothing, Move::Nothing, Move::SL,      Move::Nothing,
    Move::Nothing, Move::Nothing, Move::Nothing, Move::Nothing, Move::Nothing,
    Move::Nothing, Move::Nothing, Move::Nothing, Move::Nothing, Move::Nothing,
    Move::R,       Move::Nothing, Move::Nothing, Move::Nothing, Move::Nothing,
    Move::Nothing, Move::U,       Move::Nothing, Move::Nothing, Move::Nothing,
    Move::Nothing, Move::Nothing, Move::Nothing, Move::Nothing, Move::Nothing,
    Move::Nothing, Move::Nothing, Move::Nothing, Move::Nothing, Move::Nothing,
    Move::Nothing, Move::U,       Move::Nothing, Move::Nothing, Move::Nothing,
    Move::Nothing, Move::Nothing, Move::Nothing, Move::Nothing, Move::Nothing,
    Move::Nothing, Move::Nothing, Move::Nothing, Move::Nothing, Move::Nothing,
    Move::Nothing, Move::Nothing, Move::Nothing, Move::Nothing, Move::R,
    Move::Nothing, Move::Nothing, Move::Nothing, Move::L,       Move::Nothing,
    Move::Nothing, Move::Nothing, Move::Nothing, Move::Nothing, Move::Nothing,
    Move::Nothing, Move::Nothing, Move::Nothing, Move::Nothing, Move::Nothing,
    Move::Nothing, Move::Nothing, Move::U,       Move::Nothing, Move::Nothing,
    Move::Nothing, Move::Nothing, Move::Nothing, Move::Nothing, Move::Nothing,
    Move::Nothing, Move::Nothing, Move::Nothing, Move::Nothing, Move::Nothing,
    Move::Nothing, Move::Nothing, Move::Nothing, Move::Nothing, Move::Nothing,
    Move::R,       Move::Nothing, Move::Nothing, Move::Nothing, Move::R,
    Move::Nothing, Move::Nothing, Move::Nothing, Move::L,       Move::Nothing,
    Move::Nothing, Move::Nothing, Move::Nothing, Move::Nothing, Move::Nothing,
    Move::Nothing, Move::Nothing, Move::Nothing, Move::Nothing, Move::Nothing,
    Move::Nothing, Move::Nothing, Move::R,       Move::Nothing, Move::Nothing,
    Move::Nothing, Move::Nothing, Move::Nothing, Move::Nothing, Move::Nothing,
    Move::Nothing, Move::Nothing, Move::Nothing, Move::Nothing, Move::Nothing,
    Move::Nothing, Move::U,       Move::Nothing, Move::Nothing, Move::Nothing,
    Move::Nothing, Move::Nothing, Move::Nothing, Move::Nothing, Move::Nothing,
    Move::Nothing, Move::Nothing, Move::Nothing, Move::R,       Move::Nothing,
    Move::Nothing, Move::Nothing, Move::R,       Move::U,       Move::Nothing,
    Move::Nothing, Move::Nothing, Move::Nothing, Move::Nothing, Move::Nothing,
    Move::Nothing, Move::Nothing, Move::Nothing, Move::Nothing, Move::Nothing,
    Move::Nothing, Move::Nothing, Move::Nothing, Move::Nothing, Move::Nothing,
    Move::Nothing, Move::U};

constexpr const char* kIn = "/home/crem/dev/libsdlpop/saves/savs/lvl2.sav";
constexpr const char* kOut = "/home/crem/dev/libsdlpop/saves/savs/lvl2-moo.sav";

}  // namespace

extern "C" {
extern int interesting;
}

int main(int argc, char* argv[]) {
  Prince prince(argc, argv);
  State state;
  Scorer scorer(DefaultConfig());
  state.Quickload(kIn);

  auto saved_state_1 = state.SaveFrame();

  auto cur_level = current_level;
  // set_timer_length(timer_1, 3);
  set_timer_length(timer_1, 7);
  int frame = 0;
  std::cout << "Initial_seed " << random_seed << "\n";
  getchar();
  int last_room = 0;
  for (const auto& move : kDebugMoves) {
    std::cout << "Frame " << frame++ << " room=" << drawn_room
              << " Kid.x=" << int(Kid.x) << " Kid.y=" << int(Kid.y)
              << " Kid.frame=" << int(Kid.frame) << " Guard.x=" << int(Guard.x)
              << " Guard.y=" << int(Guard.y)
              << " Guard.frame=" << int(Guard.frame)
              << " Keypress=" << MoveToStr(move) << " seed=" << random_seed
              << std::endl;

    prince.AdvanceOneFrame(int(move));
    if (interesting) {
      std::cout << "Overflow into some room happened!";
      interesting = 0;
    }
    if (drawn_room != last_room) {
      std::cout << "Room change!\n";
      last_room = drawn_room;
    }
    // state.Quicksave("/tmp/cat/" + std::to_string(frame));
    /* std::cout << "Frame " << frame++ << " hash " << state.ComputeHash()
              << " seed " << random_seed << " move " << int(move)
              << " exit_timer:" << exit_room_timer
              << " dif_room:" << different_room; */
    // prince.PrepareGameAfterLoad();
    prince.Draw();
    /* std::cout << " exit_timer2:" << exit_room_timer
              << " iswin?:" << scorer.IsWin() << " isloss?:" << scorer.IsLoss()
              << std::endl; */
  }
  std::cout << "Levels: cur:" << cur_level << " next:" << next_level
            << std::endl;

  std::cout << "FINAL room=" << drawn_room << " Kid.x=" << int(Kid.x)
            << " Kid.y=" << int(Kid.y) << " Kid.frame=" << int(Kid.frame)
            << " Guard.x=" << int(Guard.x) << " Guard.y=" << int(Guard.y)
            << " seed=" << random_seed << std::endl;
  /* for (int i = 0; i < 30; ++i) {
    std::cout << "Skipping...";
    prince.AdvanceOneFrame(0);
    prince.Draw();
  } */

  /* if (true) {
    while (next_level == cur_level) {
      std::cout << "Waiting for the new level\n";
      prince.AdvanceOneFrame(0);
      prince.Draw();
    }

    load_lev_spr(next_level);
    load_level();
    pos_guards();
    clear_coll_rooms();
    clear_saved_ctrl();
    drawn_room = 0;
    mobs_count = 0;
    trobs_count = 0;
    next_sound = -1;
    holding_sword = 0;
    grab_timer = 0;
    can_guard_see_kid = 0;
    united_with_shadow = 0;
    flash_time = 0;
    leveldoor_open = 0;
    demo_index = 0;
    demo_time = 0;
    guardhp_curr = 0;
    hitp_delta = 0;
    Guard.charid = charid_2_guard;
    Guard.direction = dir_56_none;
    do_startpos();
    have_sword = 1;
    find_start_level_door();
    draw_level_first();
    prince.Draw();
  }
  prince.AdvanceOneFrame(0);
  prince.Draw();
  state.Quicksave(kOut); */

  getchar();

  // search.Run(12 * 60 * 30);
  // search.Run(100);
  return 0;
}
