#include "prince.h"

#include <functional>
#include <iostream>

#include "sdlpop/common.h"
#include "state.h"

extern "C" {
extern dat_type* dathandle;
extern byte* level_var_palettes;
extern int automatic_control;
}

Prince::Prince(int argc, char* argv[]) {
  g_argc = argc;
  g_argv = argv;

  random_seed = 1;
  seed_was_init = 1;

  // debug only: check that the sequence table deobfuscation did not mess things
  // up

  load_global_options();
  check_mod_param();
  load_ingame_settings();
  turn_sound_on_off(1);
  load_mod_options();

  // CusPop option
  is_blind_mode = 0;
  // Fix bug: with start_in_blind_mode enabled, moving objects are not displayed
  // until blind mode is toggled off+on??
  need_drects = 1;

  apply_seqtbl_patches();
  dathandle = open_dat("PRINCE.DAT", 0);

  /*video_mode =*/parse_grmode();

  init_timer(BASE_FPS);
  parse_cmdline_sound();

  set_hc_pal();

  current_target_surface = rect_sthg(onscreen_surface_, &screen_rect);
  show_loading();
  set_joy_mode();
  cheats_enabled = 0;
  draw_mode = 0;
  demo_mode = 0;

  init_copyprot_dialog();
  init_record_replay();

  play_demo_level = 0;

  init_menu();

  //////////////////////////////////////////////
  // init_game_main

  doorlink1_ad = /*&*/ level.doorlinks1;
  doorlink2_ad = /*&*/ level.doorlinks2;
  prandom(1);
  guard_palettes = (byte*)load_from_opendats_alloc(10, "bin", NULL, NULL);
  // (blood, hurt flash) #E00030 = red
  set_pal(12, 0x38, 0x00, 0x0C, 1);
  // (palace wall pattern) #C09850 = light brown
  set_pal(6, 0x30, 0x26, 0x14, 0);
  // Level color variations (1.3)
  level_var_palettes =
      reinterpret_cast<byte*>(load_from_opendats_alloc(20, "bin", NULL, NULL));
  // PRINCE.DAT: sword
  chtab_addrs[id_chtab_0_sword] = load_sprites_from_file(700, 1 << 2, 1);
  // PRINCE.DAT: flame, sword on floor, potion
  chtab_addrs[id_chtab_1_flameswordpotion] =
      load_sprites_from_file(150, 1 << 3, 1);
  close_dat(dathandle);
  init_lighting();
  load_all_sounds();

  hof_read();

  ///////////////////////////////////////////////////
  // start_game

  release_title_images();  // added
  free_optsnd_chtab();     // added

  start_level = 1;

  ///////////////////////////////////////////////////////////////
  // init_game

  offscreen_surface = make_offscreen_buffer(&rect_top);
  load_kid_sprite();
  text_time_remaining = 0;
  text_time_total = 0;
  is_show_time = 1;
  checkpoint = 0;
  upside_down = 0;  // N.B. upside_down is also reset in set_start_pos()
  resurrect_time = 0;
  rem_min = custom->start_minutes_left;  // 60
  rem_tick = custom->start_ticks_left;   // 719
  hitp_beg_lev = custom->start_hitp;     // 3

  need_level1_music = false;

  // play_level(1);

  ///////////////////////////////////////////////////////////////
  // play_level
  int level_number = 1;
  if (level_number != current_level) {
    load_lev_spr(level_number);
  }
  load_level();
  pos_guards();
  clear_coll_rooms();
  clear_saved_ctrl();
  drawn_room = 0;
  mobs_count = 0;
  trobs_count = 0;
  next_sound = -1;
  holding_sword = 0;
  grab_timer = 0;
  can_guard_see_kid = 0;
  united_with_shadow = 0;
  flash_time = 0;
  leveldoor_open = 0;
  demo_index = 0;
  demo_time = 0;
  guardhp_curr = 0;
  hitp_delta = 0;
  Guard.charid = charid_2_guard;
  Guard.direction = dir_56_none;
  do_startpos();
  have_sword = /*(level_number != 1)*/ (
      level_number == 0 || level_number >= custom->have_sword_from_level);
  find_start_level_door();
  // busy waiting?
  while (check_sound_playing() && !do_paused()) idle();
  stop_sounds();
  // draw_level_first();
  show_copyprot(0);
  reset_timer(timer_1);
}

Prince::~Prince() { free_peels(); }

void Prince::Run() {
  // automatic_control = 1000;
  play_level_2();
}

void Prince::AdvanceOneFrame(int value) {
  automatic_control = value;
  guardhp_delta = 0;
  hitp_delta = 0;
  timers();
  play_frame();
  automatic_control = 1000;
}

void Prince::Draw() {
  restore_room_after_quick_load();
  update_screen();
  do_simple_wait(timer_1);
}

void Prince::PrepareGameAfterLoad() {
  int temp1 = curr_guard_color;
  int temp2 = next_level;
  reset_level_unused_fields(false);
  // load_lev_spr(current_level);
  curr_guard_color = temp1;
  next_level = temp2;

  // need_full_redraw = 1;
  different_room = 1;
  // Show the room where the prince is, even if the player moved the view away
  // from it (with the H,J,U,N keys).
  next_room = drawn_room = Kid.room;
  load_room_links();
  // draw_level_first();
  // gen_palace_wall_colors();
  is_guard_notice = 0;  // prevent guard turning around immediately
  // draw_game_frame();    // for falling
  // redraw_screen(1); // for room_L

  hitp_delta = guardhp_delta = 1;  // force HP redraw
  // Don't draw guard HP if a previously viewed room (with the H,J,U,N keys) had
  // a guard but the current room doesn't have one.
  if (Guard.room != drawn_room) {
    // Like in clear_char().
    Guard.direction = dir_56_none;
    guardhp_curr = 0;
  }

  // draw_hp();
  /* loadkid_and_opp();
  // Get rid of "press button" message if kid was dead before quickload.
  text_time_total = text_time_remaining = 0;
  // next_sound = current_sound = -1;
  exit_room_timer = 0; */
}