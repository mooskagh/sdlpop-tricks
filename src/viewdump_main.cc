#include <iostream>
#include <string>
#include <utility>

#include "configs.h"
#include "search.h"
#include "serializer.h"
#include "state.h"

const auto kFilenames = {
    // "20210211-1145_F134_R7", "20210211-1145_F135_R7",
    // "20210211-1146_F136_R7",
    // "20210211-1146_F137_R7", "20210211-1147_F138_R7",
    // "20210211-1147_F139_R7",
    // "20210211-1148_F140_R7", "20210211-1148_F141_R7",
    // "20210211-1149_F142_R7",
    // "20210211-1149_F143_R7", "20210211-1150_F144_R7",
    // "20210211-1150_F145_R7",
    // "20210211-1151_F146_R7", "20210211-1151_F147_R7",
    // "20210211-1152_F148_R7",
    // "20210211-1153_F149_R7", "20210211-1153_F150_R7",
    // "20210211-1154_F151_R7",
    // "20210211-1154_F152_R7", "20210211-1155_F153_R7",
    // "20210211-1156_F154_R7",
    // "20210211-1156_F155_R7", "20210211-1157_F156_R7",
    // "20210211-1158_F157_R7",
    "20210211-1158_F158_R7", "20210211-1159_F159_R7", "20210211-1159_F160_R7",
    "20210211-1200_F161_R7", "20210211-1200_F162_R7", "20210211-1201_F163_R7",
    "20210211-1201_F164_R7", "20210211-1202_F165_R7", "20210211-1202_F166_R7",
    "20210211-1203_F167_R7", "20210211-1203_F168_R7", "20210211-1204_F169_R7",
    "20210211-1205_F170_R7",
};

int main(int argc, char* argv[]) {
  Prince prince(argc, argv);
  State state;
  std::vector<std::tuple<char_type, std::string, std::string>> states;

  for (const auto& filename : kFilenames) {
    std::cout << filename << std::flush;
    Reader reader("/home/crem/dev/libsdlpop/saves/dumps/" +
                  std::string(filename));

    state.LoadBase(reader.Read().value());

    while (auto moves = reader.Read()) {
      auto snap = reader.Read().value();
      state.LoadFrame(snap);
      if (drawn_room != 7) continue;
      states.emplace_back(Kid, snap, *moves);
    }
    std::cout << ' ' << states.size() << std::endl;
    if (states.size() >= 3000000) break;
  }

  std::sort(states.begin(), states.end(), [](const auto& x, const auto& y) {
    const auto& a = std::get<0>(x);
    const auto& b = std::get<0>(y);
    if (a.room != b.room) return a.room < b.room;
    return memcmp(&a, &b, sizeof(char_type)) < 0;
  });

  int uniq = 0;
  int largest_offset = 0;
  int largest_size = 0;
  int cur_size = 0;
  char_type cur_char;
  for (size_t i = 0; i < states.size(); ++i) {
    const auto& [x, snap, moves] = states[i];
    if (memcmp(&x, &cur_char, sizeof(char_type)) == 0) {
      ++cur_size;
    } else {
      ++uniq;
      memcpy(&cur_char, &x, sizeof(char_type));
      if (cur_size > largest_size) {
        largest_size = cur_size;
        largest_offset = i - cur_size;
      }
      cur_size = 1;
    }
  }

  std::cout << "Uniq:" << uniq << " total:" << states.size()
            << " best_off:" << largest_offset << " best_size:" << largest_size
            << std::endl;

  int i = 0;
  int skiproom = 0;
  int skipcount = 1062182;
  for (const auto& [x, snap, moves] : states) {
    state.LoadFrame(snap);
    std::cout << "R" << drawn_room << " " << i++ << "/" << states.size() << " "
              << MovesToStr(moves) << std::endl;
    if (skiproom) {
      if (skiproom != x.room) {
        skiproom = 0;
      }
    }
    if (skipcount) --skipcount;
    if (!skiproom && !skipcount) {
      prince.Draw();
      switch (getchar()) {
        case 'r':
          skiproom = x.room;
          break;
        case 't':
          skipcount = 1000;
          break;
      }
    }
  }
}