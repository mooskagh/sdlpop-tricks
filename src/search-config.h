#pragma once

#include <cstdint>
#include <optional>
#include <string>
#include <vector>

struct SearchConfig {
 public:
  enum class Direction { No, ToRight, ToLeft, ToCenter };
  enum class DoorOpen { Any, No, Yes };
  enum class Character { Kid, Guard };

  struct Rule {
    std::optional<uint8_t> room;
    std::optional<int8_t> x_from;
    std::optional<int8_t> x_to;
    std::optional<int> y_from;
    std::optional<int> y_to;
    std::optional<uint16_t> trob;
    bool must_have_guard = false;
    bool use_guard_coords = false;
    int min_prev_position = 0;
    int min_guard_hp = 0;
    int min_kid_hp = 0;

    Direction direction = Direction::No;
    DoorOpen door_open = DoorOpen::No;

    Rule& WithRoom(uint8_t);
    Rule& WithDirection(Direction);
    Rule& WithDoorOpen(DoorOpen);
    Rule& WithXRange(int8_t, int8_t);
    Rule& WithYRange(int, int);
    Rule& WithTrob(uint16_t);
    Rule& WithGuard();
    Rule& WithUseGuard();
    Rule& WithFollowing(int);
    Rule& WithMinGuardHp(int);
    Rule& WithMinKidHp(int);
  };
  struct LossRule {
    uint8_t room;
    int from_x;
    int to_x;
    int from_y;
    int to_y;
  };

  Rule& AddRule();
  Rule& AddRule(uint8_t room, Direction dir = Direction::No,
                DoorOpen door_open = DoorOpen::Any, int8_t x1 = 0,
                int8_t x2 = 9, uint8_t y_bitmask = 0x3f);
  Rule& AddRuleFollowing(uint8_t room, Direction dir = Direction::No,
                         DoorOpen door_open = DoorOpen::Any, int8_t x1 = 0,
                         int8_t x2 = 9, uint8_t y_bitmask = 0x3f);
  Rule& AddRule2(uint8_t room, Direction dir = Direction::No,
                 DoorOpen door_open = DoorOpen::Any, int8_t x1 = 0,
                 int8_t x2 = 9, int y_from = -100, int y_to = 300);
  Rule& AddRuleWithGuard(uint8_t room, Direction dir = Direction::No,
                         DoorOpen door_open = DoorOpen::Any);
  void AddLossRule(uint8_t room, uint8_t y_bitmask = 0x3f, int from_x = -100,
                   int to_x = 300);
  void AddLossRule(uint8_t room, int from_x, int to_x, int y_from, int y_to);

  std::string run_name;
  std::vector<LossRule> loss_rules;
  std::vector<Rule> rules;

  int search_width = 350000;
  float dup_score = -0.05f;
  float novelty_boost = 0.33f;
  float global_dup_score = -0.001f;
  float score_decay = 0.7f;
};

extern const std::string kRootDir;
