#include "sdlpop/common.h"

class Prince {
 public:
  Prince(int argc, char *argv[]);
  ~Prince();

  void Run();
  void AdvanceOneFrame(int value);
  void Draw();
  void PrepareGameAfterLoad();

 private:
};

/*

0 - init
1 - same
2 - steps
3 - more steps
4 - tile started to fall
5 - tile fell
6 - visited room below
7 - is below
8 - guard moved
9 - no spikes
10 - spikes
11 - gate closed
12 - gate opening
13 - gate opened
14 - gate closing
15 - gate closed


100 - not opened
101 - exit opening
102 - exit opened
103 - before exiting
104 - in process of exiting
105 - finished exiting


Scoring:
 * action score (enum actions)
 * pos score:
   * kid.x
   * kid.y
   * drawn_room
   * leveldoor_open
   * (frame_217 || next_level != current_level) == win
   * (kid.alive == 0) == loss
 * number of mobs
 * number of trobs ?

Hashing:
 * trobs (only trobs_count items)
 * mobs
 * fg
 * drawn_room
 * guards_x
 * kid
 * guard

 * kid.alive
 * hitp_curr

*/
