#pragma once

#include "search-config.h"

SearchConfig DefaultConfig();

SearchConfig Level1Config();
SearchConfig Level2Config();
SearchConfig Level3Config();
SearchConfig Level4Config();
SearchConfig Level5Config();
SearchConfig Level6Config();
SearchConfig Level7Config();
SearchConfig Level7GlitchConfig();
SearchConfig Level7FallConfig();
SearchConfig Level8Config();
SearchConfig Level9Config();
SearchConfig Level10Config();
SearchConfig Level11Config();
SearchConfig Level12Config();
SearchConfig Level13Config();
SearchConfig Level14Config();
